<?php
namespace View;

class ApiView
{
    public function __construct()
    {
        ?>
        <h1>API Stellar'isen</h1>
        <p>L'API Stellar'isen est une API permettant l'accès à des données relatives aux astres de l'univers.</p>
        <p>
        Développé par <strong><a href="https://gaelmedias.fr" style="text-decoration: none;">Gaël GRZELAK</a></strong> et <strong>Vincent FOULON</strong>,
        cette API est principalement destinée aux requêtes dynamiques de <a href="/">l'encyclopédie</a>. Cependant celle-ci n'est pas privée et peut être utilisée par d'autres développeurs.<br />
        Cette page a donc pour but de documenter son utilisation afin de vous éviter les expériences douteuses pouvant nuir à son bon fonctionnement.
        </p>
        <p>Pour autant, cette application reste un projet étudiant et nous ne garantissons pas que les données seront maintenus à jour et accessibles à long terme.</p>
        <h2>Manuel</h2>
        <p>#RTFM</p>
        <h3>Récupération des données relatives aux planètes</h3>
        <?php
        echo '<p>URL : <a href="' . $_SERVER["REQUEST_SCHEME"] . "://" . $_SERVER["HTTP_HOST"] . '/api/planets">' . $_SERVER["REQUEST_SCHEME"] . "://" . $_SERVER["HTTP_HOST"] . '/api/planets</a></p>';
        ?>
        <p>Type de retour : <a href="https://fr.wikipedia.org/wiki/JavaScript_Object_Notation">json</a></p>
        <h3>Récupération des données relatives aux satellites</h3>
        <?php
        echo '<p>URL : <a href="' . $_SERVER["REQUEST_SCHEME"] . "://" . $_SERVER["HTTP_HOST"] . '/api/planets">' . $_SERVER["REQUEST_SCHEME"] . "://" . $_SERVER["HTTP_HOST"] . '/api/satellites</a></p>';
        ?>
        <p>Type de retour : <a href="https://fr.wikipedia.org/wiki/JavaScript_Object_Notation">json</a></p>
        <h3>Récupération des données relatives au soleil</h3>
        <?php
        echo '<p>URL : <a href="' . $_SERVER["REQUEST_SCHEME"] . "://" . $_SERVER["HTTP_HOST"] . '/api/sun">' . $_SERVER["REQUEST_SCHEME"] . "://" . $_SERVER["HTTP_HOST"] . '/api/sun</a></p>';
        ?>
        <p>Type de retour : <a href="https://fr.wikipedia.org/wiki/JavaScript_Object_Notation">json</a></p>
        <h3>Récupération des données relatives à l'articles d'un astre, objet ou concept</h3>
        <?php
        echo '<p>URL : <a href="' . $_SERVER["REQUEST_SCHEME"] . "://" . $_SERVER["HTTP_HOST"] . '/api/article?type=planets&id=3">';
        echo $_SERVER["REQUEST_SCHEME"] . "://" . $_SERVER["HTTP_HOST"] . '/api/article?type=TYPE&id=ID</a></p>';
        ?>
        <p>Paramètres</p>
        <ul>
            <li>
                TYPE :
                <ul>
                    <li>planets</li>
                    <li>satellites</li>
                    <li>stars</li>
                    <li>constellations</li>
                    <li>objects</li>
                    <li>concepts</li>
                </ul>
            </li>
            <li>ID : cf. id du type de table sélectionné</li>
        </ul>
        <p>Type de retour : HTML</p>
        <?php
    }
}
