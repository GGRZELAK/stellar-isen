<?php
namespace View;

class ArticleView
{
    public function __construct($name, $article, $tags, $isFavorite)
    {
        if ($name) {
            echo "<h1>" . $name;
            if(!empty($_SESSION["user"]))
            {
                echo ' <span id="changeFavorite">';
                if ($isFavorite)
                {
                    echo '<a href="" title="Supprimer des favoris"><i class="material-icons">favorite</i></a>';
                }
                else 
                {
                    echo '<a href="" title="Ajouter aux favoris"><i class="material-icons">favorite_border</i></a>';
                }
                echo "</span>";
                echo '<span id="pdfExport"><a href="" title="Télécharger en PDF"><i class="material-icons">picture_as_pdf</i></a></span>';
            }
            echo "</h1>";
        }
        echo '<div id="tags">';
            echo '<p><a href="#" id="testtags" data-type="select2" data-pk="1" data-title="Enter tags" class="editable editable-click editable-open">'. $tags .'</a></p>';
            
            echo '<p id="updateTags">Tags : ' . $tags . "</p>";
            echo '<form method="post" action="">';
                echo '<input type="text" name="tags" placeholder="Ajouter un nouveau tag" />';
                echo '<button type="submit">Ajouter</button>';
            echo '</form>';
            
        echo '</div>';
        
        if ($article)
        {
            echo $article;
        }
        elseif ($article === 0)
        {
            echo "Erreur : Aucune donnée trouvée. Pour connaitre la liste des paramètres acceptés, merci de lire ";
            echo '<a href="/api">le manuel de notre API</a>.';
        }
        elseif ($article === false)
        {
            echo "Erreur : Veuillez renseigner un type d'astre et son id pour accéder à l'article.";
            echo "Pour connaitre la liste des paramètres acceptés, merci de lire ";
            echo '<a href="/api">le manuel de notre API</a>.';
        }
    }
}
