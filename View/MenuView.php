<?php

namespace View;

class MenuView
{
    public function __construct($en, $favorites, $planets, $satellites)
    {
        $pilePlanets = array();
        $pileSatellites = array();
        /*
        echo '<p id="takeAtour">',
            '<a href="#">',
                '<span class="iconContainer">',
                    '<i class="material-icons">play_circle_outline</i>',
                '</span>',
                '<span class="labelContainer"><strong>Faire un tour</strong></span>',
            '</a>',
        '</p>';
        */
        echo '<ul>';

        if (!empty($favorites)) {

            foreach ($favorites as $favorite) {

                if ($favorite["type"] == "Planète" and !in_array($favorite["name"], $pilePlanets) or
                    $favorite["type"] == "Satellite" and !in_array($favorite["name"], $pileSatellites)) {

                    switch ($favorite["type"]) {

                        case "Planète":
                            array_push($pilePlanets, $favorite["name"]);
                            break;

                        case "Satellite":
                            array_push($pileSatellites, $favorite["name"]);
                            break;
                    }

                    $name = (!empty($en[$favorite["name"]]["fr"])) ? $en[$favorite["name"]]["fr"] : $favorite["name"];

                    echo '<li>',
                    '<p>',
                        '<a class="showArticle" href="#' . $favorite["name"] . '">',
                    '<span class="iconContainer">',
                        '<img src="/Assets/img/object_icons/' . $favorite["name"] . '.png" alt="' . $favorite["name"] . '_icon" />',
                    '<i class="material-icons favorites">star</i>',
                    '</span>',
                    '<span class="labelContainer">',
                        '<strong>' . $name . '</strong>',
                        ' <em>' . $favorite["type"] . '</em>',
                    '</span>',
                    '</a>',
                    '</p>',
                    '</li>';
                }

                if ($favorite["type"] == "Planète" and array_key_exists($favorite["id"], $satellites)) {

                    echo '<ul>';

                    foreach ($satellites[$favorite["id"]] as $satellite) {

                        if (!in_array($satellite["name"], $pileSatellites)) {

                            array_push($pileSatellites, $satellite["name"]);

                            $name = (!empty($en[$satellite["name"]]["fr"])) ? $en[$satellite["name"]]["fr"] : $satellite["name"];

                            echo '<li>',
                            '<p>',
                                '<a class="showArticle" href="#' . $satellite["name"] . '">',
                            '<span class="iconContainer">',
                                '<img src="/Assets/img/object_icons/' . $satellite["name"] . '.png" alt="' . $satellite["name"] . '_icon" />',
                            '</span>',
                            '<span class="labelContainer">',
                                '<strong>' . $name . '</strong>',
                            ' <em>Satellite</em>',
                            '</span>',
                            '</a>',
                            '</p>',
                            '</li>';
                        }
                    }

                    echo '</ul>';
                }
            }
        }

        echo '<li>',
        '<p>',
        '<a class="showArticle" href="#Sun">',
        '<span class="iconContainer">',
        '<img src="/Assets/img/object_icons/Sun.png" alt="Sun_icon" />',
        '</span>',
        '<span class="labelContainer">',
        '<strong>Soleil</strong>',
        ' <em>Étoile</em>',
        '</span>',
        '</a>',
        '</p>',
        '</li>';

        if ($planets) {

            foreach ($planets as $planet) {

                if (!in_array($planet["name"], $pilePlanets)) {

                    array_push($pilePlanets, $planet["name"]);

                    $name = (!empty($en[$planet["name"]]["fr"])) ? $en[$planet["name"]]["fr"] : $planet["name"];

                    echo '<li>',
                    '<p>',
                        '<a class="showArticle" href="#' . $planet["name"] . '">',
                    '<span class="iconContainer">',
                        '<img src="/Assets/img/object_icons/' . $planet["name"] . '.png" alt="' . $planet["name"] . '_icon" />',
                    '</span>',
                    '<span class="labelContainer">',
                        '<strong>' . $name . '</strong>',
                    ' <em>Planète</em>',
                    '</span>',
                    '</a>',
                    '</p>',
                    '</li>';
                }

                if (array_key_exists($planet["id"], $satellites)) {

                    echo '<ul>';

                    foreach ($satellites[$planet["id"]] as $satellite) {

                        if (!in_array($satellite["name"], $pileSatellites)) {

                            array_push($pileSatellites, $satellite["name"]);

                            $name = (!empty($en[$satellite["name"]]["fr"])) ? $en[$satellite["name"]]["fr"] : $satellite["name"];

                            echo '<li>',
                            '<p>',
                                '<a class="showArticle" href="#' . $satellite["name"] . '">',
                            '<span class="iconContainer">',
                                '<img src="/Assets/img/object_icons/' . $satellite["name"] . '.png" alt="' . $satellite["name"] . '_icon" />',
                            '</span>',
                            '<span class="labelContainer">',
                                '<strong>' . $name . '</strong>',
                            ' <em>Satellite</em>',
                            '</span>',
                            '</a>',
                            '</p>',
                            '</li>';
                        }
                    }

                    echo '</ul>';
                }
            }
        }

        echo '</ul>';
    }
}
