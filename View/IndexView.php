<?php
namespace View;

use Framework\Foot;
use Framework\Head;
use Framework\Service;
use Framework\View;
use Service\ServiceLoading;

class IndexView extends View
{

public function __construct($user, $title)
{
    $this->head = new Head();
    $this->foot = new Foot();

    $this->head->setStyle("3Drendering.css", false);
    $this->head->setStyle("checkbox.css");
    $this->head->setStyle("interface.css", false);
    $this->head->setStyle("article.css");

    $this->head->setScript("three.js", false);
    $this->head->setScript("OBJLoader.js");
    $this->head->setScript("OrbitControls.js", false);
    $this->head->setScript("GLTFLoader.js");
    $this->head->setScript("xRingGeometry.js");
    $this->head->setScript("dataUniverse.js", false);
    $this->head->setScript("shaders.js", false);
    $this->head->setScript("events.js", false);
    $this->head->setScript("articleEvents.js");
    $this->head->setScript("initStars.js");
    $this->head->setScript("initConstellations.js");
    $this->head->setScript("initUniverse.js");
    $this->head->setScript("animateUniverse.js");
    $this->head->setScript("scroll.js");
    $this->head->setExternalScript("https://unpkg.com/jspdf@latest/dist/jspdf.min.js");

    $this->foot->setScript("main.js", false);

    parent::__construct($title);
?>
<header>
	<p id="logo">
		<img src="/Assets/img/logo.svg" alt="logo" /> <strong>Stellar'isen</strong>
	</p>
    <div id="search">
    	<p class="container">
    		<strong class="logo"><i class="material-icons">search</i></strong>
    		<input type="search" placeholder="Rechercher..." />
    		<a href="#" id="removeSearch">
    			<strong><i class="material-icons">close</i></strong>
    		</a>
    	</p>
        <div id="searchLoader" class="fullWidth">
            <div class="loaderContainer">
        		<?php
                $serviceLoading = new ServiceLoading();
                $serviceLoading->loading();
                ?>
                <p>recherche en cours...</p>
            </div>
        </div>
    	<div id="searchResults"></div>
    </div>
</header>

<nav>
	<p class="button" id="navigationButton">
    	<a href="#">
    		<i class="material-icons">navigation</i>
    	</a>
	</p>
	<div class="menuContainer navigationContainer">
    	<div class="menu">
    	</div>
	</div>
</nav>

<div id="settings">
    <p class="button" id="settingsButton">
    	<a href="#">
    		<i class="material-icons">settings</i>
    	</a>
    </p>
	<div class="menuContainer settingsContainer">
        <div id="user">
        	<p class="userContainer">
            	<?php
                if (! empty($user)) {

                    echo '<a href="" class="authenticate" id="button-profil">';

                    if (!empty($user["picture"])) {

                        echo '<img src="' . $user["picture"] . '" alt="Poto de profil" />';

                    } else {

                        echo '<i class="material-icons">account_circle</i>';
                    }

                    echo '<span class="name">';
                        echo $user["firstname"];
                        echo "<br />";
                        echo $user["lastname"];
                    echo "</span>";

                    echo "</a>";

                } else {

                    echo '<a href="" class="authenticate" id="button-connect">Connexion</a>';
                }
                ?>
            </p>
        </div>
        <div class="boolean">
            <p><input type="checkbox" id="constellations"><label for="constellations"><span class="ui"></span>Constellations</label></p>
            <!--<p><input type="checkbox" id="freeNavig"><label for="freeNavig"><span class="ui"></span>Navigation libre</label></p>-->
        </div>
        <div class="multiple">
        	<div id="scaleDistance">
        		<p>Echelle de distance</p>
        		<p>
        			<button name="scaleDistance" value="1">1:1</button>
        			<button name="scaleDistance" value="10">1:10</button>
        			<button name="scaleDistance" value="50">1:50</button>
        		</p>
        	</div>
        	<div id="scaleTime">
        		<p>Echelle de temps</p>
        		<p>
        			<button name="scaleTime" value="1">1</button>
        			<button name="scaleTime" value="10000">10000</button>
        			<button name="scaleTime" value="100000">100000</button>
        		</p>
        	</div>
        </div>
    </div>
</div>

<div id="pageLoader" class="fullWidth fullHeight">
    <div class="loaderContainer">
        <?php
        $serviceLoading->loading();
        ?>
        <p>Chargement en cours...</p>
    </div>
</div>

<p id="seeArticle">
    <a class="scrollTo" href="#article"><i class="material-icons">keyboard_arrow_down</i> <strong>Lire l'article</strong></a>
    <a class="scrollTop" href="#" style="display: none;"><i class="material-icons">keyboard_arrow_up</i> <strong>Retour à la 3D</strong></a>
</p>

<div id="article">
	<p class="article-header">
		<a href="#" id="close-article"><i class="material-icons">close</i></a>
	</p>
	<div class="article-body">
	</div>
</div>

<div id="popup">
    <?php
    $title = "S'identifier";
    $mail = null;
    $registerButton = "block";
    $disconnectButton = "none";
    $submit = "Se connecter";
    if (!empty($user))
    {
        $title = "Profil";
        $mail = $user["mail"];
        $registerButton = "none";
        $disconnectButton = "block";
        $submit = "Mettre à jour";
    }
    ?>
	<div id="profil">
		<div class="popup-header">
			<h3><?php echo $title; ?></h3>
			<p class="close_container">
				<a href="" class="close" id="close-profil"><i class="material-icons">close</i></a>
			</p>
		</div>
		<div class="popup-body">
            <form method="post" action="">
                <p>
                	<input placeholder="Mail" type="email" name="mail" value="<?php echo $mail; ?>" />
                </p>
                <p>
                	<input placeholder="Mot de passe" type="password" name="password" />
            	</p>
            	<p class="align-center" id="registerButton" style="display: <?php echo $registerButton; ?>;">
            		<a href="">S'enregistrer</a>
            	</p>
            	<p class="align-center" id="disconnectButton" style="display: <?php echo $disconnectButton; ?>;">
            		<a href="">Se déconnecter</a>
            	</p>
            	<p class="container-button">
            		<button type="submit"><?php echo $submit; ?></button>
            	</p>
            </form>
		</div>
	</div>
	<div id="register">
		<div class="popup-header">
			<h3>S'enregistrer</h3>
			<p class="close_container">
				<a href="" class="close" id="close-register"><i class="material-icons">close</i></a>
			</p>
		</div>
		<div class="popup-body">
			<form method="post" action="">
				<p>
					<input type="email" name="mail" placeholder="Mail" />
				</p>
				<p>
					<input type="password" name="password" placeholder="Mot de passe" />
				</p>
				<p>
					<input type="text" name="firstname" placeholder="Prénom" />
				</p>
				<p>
					<input type="text" name="lastname" placeholder="Nom" />
				</p>
				<p>
				<p class="container-button">
					<button type="submit">Créer</button>
				</p>
			</form>
		</div>
	</div>
</div>

<div id="container3Drendering"></div>
<?php
}
}
