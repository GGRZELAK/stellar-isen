<?php

namespace View;

class SearchView
{

    private function display($data, $en)
    {
        foreach ($data as $type => $content) {
            echo '<ul>';
            foreach ($content as $line) {
                echo '<li><a class="showArticle" href="#' . $line["name"] . '">';
                if (!empty($en[$line["name"]]))
                    echo $en[$line["name"]]["fr"];
                else
                    echo $line["name"];
                echo '</a> <em>';
                switch ($type) {
                    case "planets":
                        echo "Planète";
                        break;
                    case "satellites":
                        echo "Satellite";
                        break;
                    case "stars":
                        echo "étoile";
                        break;
                }
                echo '</em></li>';
            }
            echo '</ul>';
        }
    }

    public function __construct($name, $tag, $article, $dictionary)
    {
        $en = array_column($dictionary, null, "en");

        if (!$name and !$tag and !$article)
            echo "<p><strong>Aucun résultat</strong></p>";

        if ($name)
            $this->display($name, $en);

        if ($tag) {
            echo '<p class="where">Recherche par tags</p>';
            $this->display($tag, $en);
        }

        if ($article) {
            echo '<p class="where">Trouvé dans l\'article</p>';
            $this->display($article, $en);
        }
    }
}
