<?php

namespace View;

class FavoritesView
{

    public function __construct($favorite)
    {
        if (!empty($_POST)) {
            if ($favorite) {
                echo '<a href="" title="Ajouter aux favoris"><i class="material-icons">favorite_border</i></a>';
            } else {
                echo '<a href="" title="Supprimer des favoris"><i class="material-icons">favorite</i></a>';
            }
        }
    }
}
