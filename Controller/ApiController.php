<?php

namespace Controller;

use Model\AppModel;
use Framework\Controller;
use View\ArticleView;
use View\ApiView;

class ApiController extends Controller
{

    /**
     * Documentation API
     */
    public function index()
    {
        new ApiView();
    }

    /**
     * Lecture des planètes
     */
    public function planets()
    {
        $model = new AppModel();
        $planets = false;
        if (!empty($_GET["id"])) {

            $id = strip_tags($_GET["id"]);
            $planets = $model->getPlanet($id);
        } else if (!empty($_GET["idStar"])) {

            $idStar = strip_tags($_GET["idStar"]);
            $planets = $model->getPlanets($idStar);
        } else
            $planets = $model->getPlanets();

        echo json_encode($planets, JSON_NUMERIC_CHECK);
    }

    /**
     * Lecture des satellites
     */
    public function satellites()
    {
        $model = new AppModel();
        $satellites = false;
        if (!empty($_GET["id"])) {
            $id = strip_tags($_GET["id"]);
            $satellites = $model->getSatellite($id);
        } else if (!empty($_GET["idPlanet"])) {
            $idPlanet = strip_tags($_GET["idPlanet"]);
            $satellites = $model->getSatellites($idPlanet);
        } else
            $satellites = $model->getSatellites();

        echo json_encode($satellites, JSON_NUMERIC_CHECK);
    }

    /**
     * Lecture du soleil
     */
    public function sun()
    {
        $model = new AppModel();
        $sun = false;
        $idStar = (!empty($_GET["idStar"])) ? strip_tags($_GET["idStar"]) : 0;
        $sun = $model->getSun($idStar);

        echo json_encode($sun, JSON_NUMERIC_CHECK);
    }

    /**
     * Lecture des étoiles
     */
    public function stars()
    {
        $model = new AppModel();
        $stars = false;
        $nbStars = false;

        if (!empty($_GET["min"]) AND !empty($_GET["max"])) {

            $data["min"] = strip_tags($_GET["min"]);
            $data["max"] = strip_tags($_GET["max"]);
            $stars = $model->getStars($data);

        } else if (!empty($_GET["hip"])) {

            $data["min"] = strip_tags($_GET["hip"]);
            $data["max"] = strip_tags($_GET["hip"]);
            $stars = $model->getStars($data);
        } else {

            $nbStars = $model->countStars();
        }

        if ($nbStars) echo $nbStars;
        else if ($stars) echo json_encode($stars, JSON_NUMERIC_CHECK);
    }

    /**
     * Lecture d'un article
     */
    public function article()
    {
        $model = new AppModel();
        $name = false;
        $article = false;
        $tags = false;
        $isFavorite = false;

        if (!empty($_GET["type"]) && isset($_GET["id"])) {

            $type = strip_tags($_GET["type"]);
            $id = strip_tags($_GET["id"]);

            $dictionary = $model->getDictionary();
            $en = array_column($dictionary, null, "en");
            $name = $model->getName($type, $id);
            $name = (!empty($en[$name]["fr"])) ? $en[$name]["fr"] : $name;
            $article = $model->getArticle($type, $id);
            $tags = $model->getTags($type, $id);

            // si une session est ouverte

            if (!empty($_SESSION["user"])) {

                $data = array();

                $data["id"] = $id;
                $data["type"] = $type;
                $data["user"] = $_SESSION["user"];

                // vérifie si l'article ouvert fait partie des favoris de l'utilisateur

                $isFavorite = $model->getFavorite($data);
            }
        }

        new ArticleView($name, $article, $tags, $isFavorite);
    }

    /**
     * Lecture des constellations
     */
    public function constellations()
    {
        $model = new AppModel();
        $constellation = false;

        if (!empty($_GET["id"])) {
            $id = strip_tags($_GET["id"]);
            $constellation = $model->getConstellationById($id);
        } elseif (!empty($_GET["con"])) {
            $con = strip_tags($_GET["con"]);
            $constellation = $model->getConstellationByCon($con);
        } else {
            $constellation = $model->getConstellations();
        }

        echo json_encode($constellation, JSON_NUMERIC_CHECK);
    }

    /**
     * Gestion des tags
     */
    public function tags()
    {
        $model = new AppModel();
        $tags = false;

        if (!empty($_POST['tags']) and !empty($_POST["type"]) and !empty($_POST["id"])) {

            $data = array();

            $type = strip_tags($_POST["type"]);
            $id = strip_tags($_POST["id"]);

            $tags = $model->getTags($type, $id);
            if (!empty($tags))
                $tags .= ", ";
            $tags = $tags . $_POST['tags'];

            $data["tags"] = $tags;
            $data["id"] = $id;

            $model->updateTags($type, $data);
        }

        echo "Tags : " . $tags;
    }
}

