<?php
namespace Controller;

use Model\AppModel;
use View\FavoritesView;

class UserController
{

    /**
     * Gestion des Favoris
     */
    public function favorites()
    {
        $model = new AppModel();

        $favorite = false;

        if (! empty($_SESSION["user"])) {
            $data = array();

            $data["user"] = $_SESSION["user"];

            if (! empty($_POST["type"]) and ! empty($_POST["id"])) {
                $data["type"] = strip_tags($_POST["type"]);
                $data["id"] = strip_tags($_POST["id"]);

                $favorite = $model->getFavorite($data);

                if (! empty($favorite))
                    $model->deleteFavorite($data);
                else
                    $model->setFavorite($data);
            }
        }

        new FavoritesView($favorite);
    }
}
