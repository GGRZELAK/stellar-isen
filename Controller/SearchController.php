<?php
namespace Controller;

use Model\AppModel;
use View\SearchView;

class SearchController
{
    /**
     * Empile les résultats de la recherche au fur et à mesure
     * @param array $data
     * @param array $array
     * @return array ou false si aucun résultat
     */
    private function saveData($data, $array)
    {
        foreach ($array as $type => $content)
        {
            if (!empty($content))
            {
                if (!isset($data[$type])) $data[$type] = array();
                foreach ($content as $line)
                {
                    if (!in_array($line["id"], $data[$type])) array_push($data[$type], $line["id"]);
                }
            }
        }
        return $data;
    }
    
    /**
     * Supprime les résultats vides ou qui ont déja été proposé
     * @param array $data
     * @param array $array
     */
    private function unique($data, $array)
    {
        foreach ($array as $type => $content)
        {
            if (!empty($content))
            {
                if (!empty($data[$type]))
                {
                    foreach ($content as $keyLine => $line)
                    {
                        if (in_array($line["id"], $data[$type])) unset($array[$type][$keyLine]);
                    }
                }
            }
            else unset($array[$type]);
        }
        return $array;
    }
    
    /**
     * Recherche
     * Trié par importance de découverte : nom, tags, contenu dans l'article
     */
    public function index()
    {
        $model = new AppModel();

        $data = array();

        $name = false;
        $tag = false;
        $article = false;

        $dictionary = $model->getDictionary();
        $fr = array_column($dictionary, null, "fr");
        
        if (!empty($_GET["name"]))
        {
            $name = mb_strtolower(strip_tags($_GET["name"]));
            if (!empty($fr[$name])) $name = $fr[$name]["en"];
            $name = $this->unique($data, $model->searchByName($name));
            $data = $this->saveData($data, $name);
        }
        
        if (!empty($_GET["tag"]))
        {
            $tag = strip_tags($_GET["tag"]);
            $tag = $this->unique($data, $model->searchByTags($tag));
            $data = $this->saveData($data, $tag);
        }
        
        if (!empty($_GET["article"]))
        {
            $search = strip_tags($_GET["article"]);
            $article = $this->unique($data, $model->searchInArticle($search));
        }

        new SearchView($name, $tag, $article, $model->getDictionary(true));
    }
}
