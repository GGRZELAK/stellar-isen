<?php
namespace Controller;

use Framework\App;

class ScriptController
{

    public function index()
    {
        header('Content-Type: text/javascript');
        $app = new App();
        foreach ($app as $keyRule => $valueRule) {
            echo "var " . $keyRule . " = " . '"' . $valueRule . '";' . "\n";
        }
    }
}

