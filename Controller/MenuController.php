<?php
namespace Controller;

use Model\AppModel;
use View\MenuView;

class MenuController
{

    /**
     * Gestion du menu de Navigation
     */
    public function navigation()
    {
        $model = new AppModel();

        // Favoris

        $en = false;
        $favorites = false;
        $planets = false;
        $satellites = false;

        $dictionary = $model->getDictionary();
        $en = array_column($dictionary, null, "en");
        
        $planets = array_column($model->getPlanets(), null, "id");
        $satellites = array_column($model->getSatellites(), null, "id");

        if (! empty($_SESSION["user"])) {

            $favorites = array();

            $favorites = $model->getFavorites($_SESSION["user"]);

            foreach ($favorites as $key => $favorite) {

                switch ($favorite["referencetable"]) {

                    case "stars":
                        $favorites[$key] = $model->getStar($favorite["idelement"]);
                        $favorites[$key]["type"] = "Étoile";
                        break;

                    case "planets":
                        $favorites[$key] = $planets[$favorite["idelement"]];
                        $favorites[$key]["type"] = "Planète";
                        break;

                    case "satellites":
                        $favorites[$key] = $satellites[$favorite["idelement"]];
                        $favorites[$key]["type"] = "Satellite";
                        break;
                }

                $favorites[$key]["date"] = $favorite["date"];
            }
        }

        // Récupération de l'astre courrant
        $idPlanet = 3;

        if (! empty($_GET["type"]) and ! empty($_GET["id"])) {

            $type = strip_tags($_GET["type"]);
            $id = strip_tags($_GET["id"]);

            if ($type == "planets")
                $idPlanet = $id;
            else if ($type == "satellites" AND !empty($satellites[$id]))
                $idPlanet = $satellites[$id]["idplanet"];
        }
        
        if (! empty($planets[$idPlanet]))
            $idStar = $planets[$idPlanet]["idstar"];

        // Planètes

        $planets = $model->getPlanets($idStar);

        // Satellites

        $satellites = array(
            $idPlanet => $model->getSatellites($idPlanet)
        );

        new MenuView($en, $favorites, $planets, $satellites);
    }
}
