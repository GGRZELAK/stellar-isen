<?php
namespace Controller;

use Framework\Controller;
use Model\AppModel;
use View\IndexView;

class IndexController extends Controller
{
    public function index()
    {
        $model = new AppModel();
        $user = null;
        
        if (! empty($_SESSION["user"])) $user = $model->getUser($_SESSION["user"]);

        $conf = (file_exists("Framework/prod.ini")) ?  parse_ini_file("Framework/prod.ini", true) : parse_ini_file("Framework/dev.ini", true);

        new IndexView($user, $conf["APP"]["name"]);
    }
}
