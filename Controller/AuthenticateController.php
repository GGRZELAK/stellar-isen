<?php
namespace Controller;

use Model\AppModel;

class AuthenticateController
{
    // Identification et affichage des informations utilisateur
    
    public function index()
    {
        $json = array();
        
        $json["success"] = false;
        $json["error"] = false;
        
        $model = new AppModel();

        if ((!empty($_POST['mail']) || !empty($_GET["mail"])) && (!empty($_POST['password']) || !empty($_GET["password"])))
        {
            $mail = (!empty($_POST['mail'])) ? strip_tags($_POST['mail']) : strip_tags($_GET['mail']);
            $password = (!empty($_POST['password'])) ? strip_tags($_POST['password']) : strip_tags($_GET['password']);
            
            $user = $model->getUserByMail($mail);
            
            if (!empty($user))
            {
                if ($user["password"] == $password)
                {
                    $_SESSION["user"] = $user["id"];
                }
                else
                {
                    $json["error"] = "Le mot de passe saisi est incorrect.";
                }
            }
            else
            {
                $json["error"] = "Le mail saisi est incorrect.";
            }
        }
        else
        {
            $json["error"] = "Veuillez remplir tous les champs.";
        }
        
        if (!empty($_SESSION["user"]))
        {
            $json["success"] = $model->getUser($_SESSION["user"]);
        }
        else if (empty($json["error"]))
        {
            $json["error"] = "Vous n'êtes pas connecté";
        }

        echo json_encode($json, JSON_NUMERIC_CHECK);
    }
   
    //Création d'un utilisateur
    
    public function create()
    {
        $json = array();
        
        $json["success"] = false;
        $json["error"] = false;
        
        if ((!empty($_POST['mail'])         || !empty($_GET['mail']))       &&
            (!empty($_POST['password'])     || !empty($_GET['password']))   &&
            (!empty($_POST['firstname'])    || !empty($_GET['firstname']))  &&
            (!empty($_POST['lastname'])     || !empty($_GET['lastname'])))
        {
            $model = new AppModel();
            
            $data = array();
            
            $data["mail"] = (!empty($_POST['mail'])) ? strip_tags($_POST['mail']) : strip_tags($_GET['mail']);
            $data["password"] = (!empty($_POST['password'])) ? strip_tags($_POST['password']) : strip_tags($_GET['password']);
            $data["firstname"] = (!empty($_POST['firstname'])) ? strip_tags($_POST['firstname']) : strip_tags($_GET['firstname']);
            $data["lastname"] = (!empty($_POST['lastname'])) ? strip_tags($_POST['lastname']) : strip_tags($_GET['lastname']);
            
            $users = $model->getUsers();            
            $mails = array_column($users, "mail");
            
            if (!in_array($data["mail"], $mails))
            {
                $id = $model->setUser($data);
                
                if (!empty($id))
                {
                    $user = $model->getUser($id);
                    if (!empty($user)) $json["success"] = $user;
                }
                else $json["error"] = "La création de l'utilisateur a échoué.";
            }
            else $json["error"] = "Le mail que vous avez saisi est déjà enregistré.";
        }
        else $json["error"] = "Veuillez remplir tous les champs.";

        echo json_encode($json, JSON_NUMERIC_CHECK);
    }
    
    //Suppression d'un utilisateur
    
    public function remove()
    {
        $json = array();
        
        $json["success"] = false;
        $json["error"] = false;
        
        if (!empty($_SESSION["user"]))
        {
            if (!empty($_POST["id"]) OR !empty($_GET["id"]))
            {
                $id = (!empty($_POST["id"])) ? strip_tags($_POST["id"]) : strip_tags($_GET["id"]);
                
                $model = new AppModel();
                
                $success = false;
                
                if ($_SESSION["user"] == $id)
                {
                    $success = true;
                }
                else
                {
                    $user = $model->getUser($_SESSION["user"]);
                    
                    if (!empty($user) AND $user["administrator"])
                    {
                        $success = true;
                    }
                    else
                    {
                        $json["error"] = "Vous ne pouvez pas supprimer cet utilisateur.";
                    }
                }
                
                if ($success)
                {                    
                    $return = $model->deleteUser($id);
                    
                    if (!empty($return))
                    {
                        $json["success"] = "L'utilisateur a bien été supprimé.";
                    }
                    else
                    {
                        $json["error"] = "Aucun utilisateur n'a été supprimé.";
                    }
                }
            }
            else
            {
                $json["error"] = "Veuillez indiquer l'ID de l'utilisateur à supprimer.";
            }
        }
        else
        {
            $json["error"] = "Vous devez être connecté pour supprimer un utilisateur.";
        }
        
        echo json_encode($json, JSON_NUMERIC_CHECK);
    }
    
    //Mise à jour des informations utilisateur
    
    public function update()
    {
        $model = new AppModel();
        
        $json = array();
        
        $json["success"] = false;
        $json["error"] = false;
        
        if (!empty($_SESSION["user"]))
        {
            if ((!empty($_POST['mail'])     || !empty($_GET['mail']))   &&
                (!empty($_POST['password']) || !empty($_GET['password'])))
            {
                $id = $_SESSION["user"];
                if (!empty($_POST["id"])) $id = strip_tags($_POST["id"]);
                else if (!empty($_GET["id"])) $id = strip_tags($_GET["id"]);
                
                $success = false;
                
                if ($_SESSION["user"] == $id)
                {
                    $success = true;
                }
                else
                {
                    $user = $model->getUser($_SESSION["user"]);
                    
                    if (!empty($user) AND $user["administrator"])
                    {
                        $success = true;
                    }
                    else
                    {
                        $json["error"] = "Vous ne pouvez pas modifier cet utilisateur.";
                    }
                }
                
                if ($success)
                {
                    $mail = (!empty($_POST["mail"])) ? strip_tags($_POST["mail"]) : strip_tags($_GET["mail"]);
                    $password = (!empty($_POST["password"])) ? strip_tags($_POST["password"]) : strip_tags($_GET["password"]);
                    
                    $data = array();
                    
                    $data["id"] = $id;
                    $data["mail"] = $mail;
                    $data["password"] = $password;
                    
                    $return = $model->updateUser($data);
                    
                    if (!empty($return))
                    {
                        $json["success"] = "Les informations de l'utilisateur ont bien été modifiées.";
                    }
                    else
                    {
                        $json["error"] = "Les informations de l'utilisateur n'ont pas été mises à jours.";
                    }
                }
            }
            else
            {
                $json["error"] = "Veuillez remplir tous les champs";
            }
        }
        else
        {
            $json["error"] = "Vous devez être connecté pour modifier les informations d'un utilisateur.";
        }

        echo json_encode($json, JSON_NUMERIC_CHECK);
    }
    
    public function disconnect()
    {
        session_destroy();
    }
}
