<?php
namespace Controller\System;

use Model\System\IndexingModel;
use Framework\App;

class IndexingController
{

    private $model;

    private $app;

    public function __construct()
    {
        $this->model = new IndexingModel();
        $this->app = new App();
    }

    /**
     * Indexation de l'userAgent
     *
     * @return int $agentId Un ID correspondant à un userAgent
     */
    public function agent()
    {
        $agentId = $this->model->getAgentId($_SERVER["HTTP_USER_AGENT"]);

        if (empty($agentId)) {

            $agentId = $this->model->setAgent($_SERVER["HTTP_USER_AGENT"]);
        }

        return $agentId;
    }

    /**
     * Indexation de l'IP
     *
     * @return int $ipId Un ID correspondant à une IP
     */
    public function ip()
    {
        $ipId = $this->model->getIpId($_SERVER["REMOTE_ADDR"]);

        if (empty($ipId)) {

            $ipId = $this->model->setIp($_SERVER["REMOTE_ADDR"]);
        }

        return $ipId;
    }

    /**
     * Indexation du périphérique
     *
     * @return int $deviceId Un ID correspondant à un identifiant unique de péruphérique
     */
    public function device()
    {
        if (empty($_COOKIE["device"])) {

            $hash = uniqid();
            $expireTime = time() + 5 * 365 * 24 * 3600;
            setcookie("device", $hash, $expireTime, '/', '.' . $this->app->dns);
        } else {

            $hash = strip_tags($_COOKIE["device"]);
        }

        $deviceId = $this->model->getDevice($hash);

        if (empty($deviceId)) {

            $data = array();
            $data["hash"] = $hash;
            $data["expireTime"] = date("Y-m-d H:i:s", time() + 5 * 365 * 24 * 3600);

            $deviceId = $this->model->setDevice($data);
        }

        return $deviceId;
    }

    /**
     * Indexation des Pages
     *
     * @return int $pageId Un ID correspondant à une page visitée
     */
    public function page()
    {
        $data = array();
        $data["subDns"] = $GLOBALS["subDns"];
        $data["url"] = $_SERVER['REQUEST_URI'];

        $pageId = $this->model->getPageId($data);

        if (empty($pageId)) {

            $pageId = $this->model->setPage($data);
        }

        return $pageId;
    }
}
