<?php
namespace Framework;

class Foot
{
    private $script = array();
    private $externalScript = array();
    private $app;
    
    public function __construct()
    {
        $this->app = new App();
    }
    
    /**
     * Ajoute des script dans le DOM
     */
    public function generateFoot()
    {
        $this->getScript();
    }

    /**
     * Injecte les script
     */
    private function getScript()
    {
        // CDN Script
        for ($currScript = 0; $currScript < count($this->externalScript); $currScript++)
        {
            echo '<script src="', $this->externalScript[$currScript]["file"], '" type="text/javascript"';
            if ($this->externalScript[$currScript]["async"]) echo " async";
            echo '></script>', "\n";
        }

        // Script local
        for ($currScript = 0; $currScript < count($this->script); $currScript++)
        {
            echo '<script src="/' . $this->app->asset . $this->app->script, $this->script[$currScript]["file"], '?', filemtime("./" . $this->app->asset . $this->app->script . $this->script[$currScript]["file"]), '" type="text/javascript"';
            if ($this->script[$currScript]["async"]) echo " async";
            echo '></script>', "\n";
        }
    }

    /**
     * Ajoute un script local dans le header
     * @param string $script
     * @param bool $async Chargement asynchrone, actif par défaut.
     */
    public function setScript($script, $async = true)
    {
        array_push($this->script, array("file" => $script, "async" => $async));
    }

    /**
     * Ajoute un script depuis un CDN dans le header
     * @param string $script
     * @param bool $async Chargement asynchrone, actif par défaut.
     */
    public function setExternalScript($script, $async = true)
    {
        array_push($this->externalScript, array("file" => $script, "async" => $async));
    }
}
