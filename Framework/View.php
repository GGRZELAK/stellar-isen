<?php
namespace Framework;

class View
{
    public $app;
    public $head;
    public $foot;
    
    public function __construct($title)
    {
        $this->app = new App();
        
        echo '<!DOCTYPE html>', "\n";
        echo '<html>', "\n";
        
        if (empty($this->head)) $this->head = new Head();
        
        $this->head->setTitle($title);
        $this->head->setFavicon('/' . $this->app->asset . $this->app->image . "favicon.svg");
        
        $this->head->setIcon("https://fonts.googleapis.com/icon?family=Material+Icons");
        $this->head->setIcon("https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css");

        $this->head->setFont("https://fonts.googleapis.com/css?family=Roboto+Condensed");
        $this->head->setFont("https://fonts.googleapis.com/css?family=Eczar");

        $this->head->setExternalScript("https://code.jquery.com/jquery-3.4.1.min.js", false);
        $this->head->setExternalScript("https://code.jquery.com/ui/1.12.1/jquery-ui.min.js", false);
        //$this->head->setExternalScript("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js", false);
        //$this->head->setExternalScript("https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.15.0/umd/popper.min.js", false);
        //$this->head->setExternalScript("https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js", false);
        //$this->head->setExternalScript("https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js");
        
        $this->head->setExternalScript($this->app->protocol . $this->app->dns . "/script", false);

        $this->head->setScript("jquery.growl.js");
        
        //$this->head->setExternalStyle("https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css", false);
        //$this->head->setExternalStyle("https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css");

        $this->head->setStyle("global.css", false);
        $this->head->setStyle("mediaQueries.css", false);
        $this->head->setStyle("jquery.growl.css");
        
        $this->head->generateHead();
        
        echo '<body>', "\n";
    }
    
    public function __destruct()
    {
        if (empty($this->foot)) $this->foot = new Foot();
        $this->foot->generateFoot();
        echo "\n", "</body>";
    }
}
