<?php

namespace Framework;

use Controller\System\FilterController;
use Controller\System\IndexingController;
use Controller\System\LogController;

class Controller
{

    public $ici;

    public $ou;

    public function __construct()
    {
        $app = new App();

        $indexing = new IndexingController();
        $filter = new FilterController();
        $log = new LogController();

        // URL en cours
        $this->ici = $app->protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

        // Emplacement de redirection
        if (!empty($_GET['redirect'])) {
            $this->ou = strip_tags($_GET['redirect']);
        } else {
            $this->ou = $app->protocol . $app->dns . '/';
        }

        // Indexation
        $agentId = $indexing->agent();
        $externalIpId = $indexing->ip();
        $deviceId = $indexing->device();
        $pageId = $indexing->page();

        // Logs
        $log->deviceAgent($deviceId, $agentId);
        $log->ipDevice($deviceId, $externalIpId);
        $log->ipAgent($agentId, $externalIpId);

        if (!empty($_SESSION["userId"])) {

            $log->userAgent($agentId);
            $log->userDevice($deviceId);
            $log->userIp($externalIpId);
        }

        $log->history($pageId, $deviceId, $agentId, $externalIpId);

        // Filtrage
        $filter->blacklist($pageId, $deviceId, $agentId, $externalIpId);
    }
}
