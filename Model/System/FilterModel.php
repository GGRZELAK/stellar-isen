<?php
namespace Model\System;

use Framework\Model;
use PDO;

class FilterModel extends Model
{
    function getBlacklist($data)
    {
        $sql = "SELECT * FROM blacklist WHERE
            pageId = :pageId OR NULL AND
            externalipid = :externalIpId OR NULL AND
            localipid = :localIpId OR NULL AND
            agentid = :agentId OR NULL AND
            userid = :userId OR NULL AND
            deviceid = :deviceId OR NULL";
        
        $req = $this->executerRequete($sql, $data);
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }
    
    function setBlacklist($data)
    {
        $sql = 'INSERT INTO blacklist(pageid, userid, deviceid, externalipid, localipid, agentid) VALUES(?, ?, ?, ?, ?, ?)';
        return $this->executerRequete($sql, $data);
    }
}
