<?php
namespace Model\System;

use Framework\Model;
use PDO;

class IndexingModel extends Model
{

    function getAgentId($agent)
    {
        $sql = "SELECT id FROM agent WHERE name = ?";

        $req = $this->executerRequete($sql, array(
            $agent
        ));
        $output = $req->fetch(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output["id"];
    }

    function getIpId($ip)
    {
        $sql = "SELECT id FROM ip WHERE ip = ?";

        $req = $this->executerRequete($sql, array(
            $ip
        ));
        $output = $req->fetch(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output["id"];
    }

    function getDevice($hash)
    {
        $sql = "SELECT * FROM devices WHERE hash = ?";
        $req = $this->executerRequete($sql, array(
            $hash
        ));
        $output = $req->fetch(PDO::FETCH_NAMED);
        $req->closeCursor();
        return $output["id"];
    }

    function getPageId($data)
    {
        $sql = 'SELECT * FROM pages WHERE subdns = :subDns  OR (subdns IS NULL AND :subDns IS NULL) AND url = :url';
        $req = $this->executerRequete($sql, $data);
        $output = $req->fetch(PDO::FETCH_NAMED);
        $req->closeCursor();
        return $output['id'];
    }

    function setAgent($agent)
    {
        $sql = 'INSERT INTO agent(name) VALUES(?)';
        return $this->executerRequete($sql, array(
            $agent
        ));
    }

    function setIp($ip)
    {
        $sql = 'INSERT INTO ip(ip) VALUES(?)';
        return $this->executerRequete($sql, array(
            $ip
        ));
    }

    function setDevice($data)
    {
        $sql = 'INSERT INTO devices(hash, expiretime) VALUES(:hash, :expireTime)';
        return $this->executerRequete($sql, $data);
    }

    function setPage($data)
    {
        $sql = 'INSERT INTO pages(subdns, url) VALUES(:subDns, :url)';
        return $this->executerRequete($sql, $data);
    }
}
