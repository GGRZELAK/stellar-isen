<?php
namespace Model\System;

use Framework\Model;
use PDO;

class GenericModel extends Model
{
    public function delData($table, $id)
    {
        $sql = "DELETE FROM " . $table . " WHERE id = ?";
        $response = $this->executerRequete($sql, array($id), 1);
        
        return (is_object($response)) ? $response->rowCount() : $response;
    }

    public function updData($table, $values, $where)
    {
        $sql = "UPDATE " . $table . " SET ";
        $string = array();
        foreach (array_keys($values) as $column) {

            array_push($string, $column . " = :" . $column);
        }
        $id = array_keys($where)[0];
        $sql .= implode(', ', $string) . " WHERE " . $id . " = :" . $id;
        
        $response = $this->executerRequete($sql, array_merge($values, $where), 1);

        if (is_object($response)) {

            return $response->rowCount();
        } else {

            return $response;
        }
    }

    public function getStruct($table)
    {
        $sql = "SHOW COLUMNS FROM " . $table;

        $req = $this->executerRequete($sql);
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getForeignKey($table)
    {
        $sql = "SELECT
                  TABLE_NAME,
                  COLUMN_NAME,
                  CONSTRAINT_NAME,
                  REFERENCED_TABLE_NAME,
                  REFERENCED_COLUMN_NAME
                FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
                WHERE
                  TABLE_NAME = '" . $table . "' AND REFERENCED_TABLE_NAME IS NOT NULL;";

        $req = $this->executerRequete($sql);
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    public function getData($table, $select = array(), $where = array(), $operator = array(), $order = array(), $limit = null, $offset = null)
    {
        $sql = "SELECT ";

        if (! empty($select)) {

            $sql .= implode(', ', $select);
        } else {

            $sql .= '*';
        }

        $sql .= " FROM " . $table;

        if (! empty($where)) {

            $whereKeys = array_keys($where);

            $sql .= " WHERE ";

            $numWhereKeys = count($whereKeys);

            for ($count = 0; $count < $numWhereKeys; $count ++) {
                $sql .= ':' . $whereKeys[$count] . " = ?";
                if ($count < $numWhereKeys - 1) {
                    if (! empty($operator[$count])) {
                        $sql .= ' ' . $operator[$count] . ' ';
                    } else {
                        $sql .= " AND ";
                    }
                }
            }
        }

        if (! empty($order)) {

            $sql .= " ORDER BY ";

            $orderArray = array();
            foreach ($order as $column => $direction) {
                array_push($orderArray, $column . ' ' . $direction);
            }
            $sql .= implode(', ', $orderArray);
        }

        if (! empty($limit)) {

            $sql .= " LIMIT " . $limit;
        }

        if (! empty($offset)) {

            $sql .= " OFFSET " . $offset;
        }

        $req = $this->executerRequete($sql, $where);
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }
}

