<?php
namespace Model\System;

use Framework\Model;
use PDO;

class LogModel extends Model
{

    function getDeviceAgent($data)
    {
        $sql = 'SELECT * FROM devicesagent WHERE
                deviceid = :deviceId AND
                agentid = :agentId';
        $req = $this->executerRequete($sql, $data);
        $output = $req->fetch(PDO::FETCH_NAMED);
        $req->closeCursor();
        return $output;
    }

    function getIpDevices($data)
    {
        $sql = 'SELECT * FROM ipdevices WHERE
                deviceid = :deviceId AND
                externalipid = :externalIpId AND
                localipid = :localIpId OR (localipid IS NULL AND :localIpId IS NULL)';
        $req = $this->executerRequete($sql, $data);
        $output = $req->fetch(PDO::FETCH_NAMED);
        $req->closeCursor();
        return $output;
    }

    function getIpAgent($data)
    {
        $sql = 'SELECT * FROM ipagent WHERE
                agentid = :agentId AND
                externalipid = :externalIpId AND
                localipid = :localIpId OR (localipid IS NULL AND :localIpId IS NULL)';
        $req = $this->executerRequete($sql, $data);
        $output = $req->fetch(PDO::FETCH_NAMED);
        $req->closeCursor();
        return $output;
    }

    function getUserAgent($data)
    {
        $sql = 'SELECT * FROM usersagent WHERE
                userid = :userId AND
                agentid = :agentId';
        $req = $this->executerRequete($sql, $data);
        $output = $req->fetch(PDO::FETCH_NAMED);
        $req->closeCursor();
        return $output;
    }

    function getUserDevice($data)
    {
        $sql = 'SELECT * FROM usersdevices WHERE
                userid = :userId AND
                deviceid = :deviceId';
        $req = $this->executerRequete($sql, $data);
        $output = $req->fetch(PDO::FETCH_NAMED);
        $req->closeCursor();
        return $output;
    }

    function getUserIp($data)
    {
        $sql = 'SELECT * FROM usersip WHERE
                userid = :userId AND
                externalipid = :externalIpId AND
                localipid = :localIpId OR (localipid IS NULL AND :localIpId IS NULL)';
        $req = $this->executerRequete($sql, $data);
        $output = $req->fetch(PDO::FETCH_NAMED);
        $req->closeCursor();
        return $output;
    }

    function setDeviceAgent($data)
    {
        $sql = "INSERT IGNORE INTO devicesagent(deviceid, agentid) VALUES(:deviceId, :agentId)";
        return $this->executerRequete($sql, $data)->rowCount();
    }

    function setIpDevices($data)
    {
        $sql = "INSERT IGNORE INTO ipdevices(deviceid, externalipid, localipid) VALUES(:deviceId, :externalIpId, :localIpId)";
        return $this->executerRequete($sql, $data)->rowCount();
    }

    function setIpAgent($data)
    {
        $sql = "INSERT IGNORE INTO ipagent(agentid, externalipid, localipid) VALUES(:agentId, :externalIpId, :localIpId)";
        return $this->executerRequete($sql, $data)->rowCount();
    }

    function setUserAgent($data)
    {
        $sql = "INSERT IGNORE INTO usersagent(userid, agentid) VALUES(:userId, :agentId)";
        return $this->executerRequete($sql, $data)->rowCount();
    }

    function setUserDevice($data)
    {
        $sql = "INSERT IGNORE INTO usersdevices(userid, deviceid) VALUES(:userId, :deviceId)";
        return $this->executerRequete($sql, $data)->rowCount();
    }

    function setUserIp($data)
    {
        $sql = "INSERT IGNORE INTO usersip(userid, externalipid, localipid) VALUES(:userId, :externalIpId, :localIpId)";
        return $this->executerRequete($sql, $data)->rowCount();
    }
    
    function history($data)
    {
        $sql = "INSERT INTO history(pageid, deviceid, userid, agentid, externalipid, localipid) VALUES(:pageId, :deviceId, :userId, :agentId, :externalIpId, :localIpId)";
        return $this->executerRequete($sql, $data);
    }
}
