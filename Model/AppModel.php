<?php
namespace Model;

use Framework\Model;
use PDO;
use PDOException;

class AppModel extends Model
{
    /**
     * Création d'un utilisateur
     * @param array $data : pseudo, password, mail
     * @return string ID du nouvel utilisateur
     */
    function setUser($data)
    {
        $sql = "INSERT INTO users(firstname, lastname, mail, password) VALUES(:firstname, :lastname, :mail, :password)";
        return $this->executerRequete($sql, $data);
    }

    /**
     * Suppression d'un utilisateur
     * @param int $id : ID de l'utilisateur
     * @return string Nombre de lignes affecté
     */
    function deleteUser($id)
    {
        $sql = "DELETE FROM users WHERE id = :id";
        return $this->executerRequete($sql, array("id" => $id));
    }

    /**
     * Met à jour les informations d'un utilisateur
     * @param array $data : id, mail, password
     * @return string Nombre d'utilisateurs affectés
     */
    function updateUser($data)
    {
        $sql = "UPDATE users SET mail = :mail, password = :password WHERE id = :id";
        return $this->executerRequete($sql, $data);
    }

    /**
     * Ajout d'un Favori
     * @param array $data : user, type, id
     * @return string ID du Favori inséré
     */
    function setFavorite($data)
    {
        $sql = "INSERT INTO favorites(iduser, referencetable, idelement) VALUES(:user, :type, :id)";
        return $this->executerRequete($sql, $data);
    }

    /**
     * Suppression d'un Favori
     * @param array $data : user, type, id
     * @return string Nombre de lignes supprimées
     */
    function deleteFavorite($data)
    {
        $sql = "DELETE FROM favorites WHERE iduser = :user AND referencetable = :type AND idelement = :id";
        return $this->executerRequete($sql, $data);
    }

    /**
     * Lecture d'un Favori
     * @param array $data : user, type, id
     * @return string Retourne le Favori si celui-ci existe, sinon retourne une chaine vide
     */
    function getFavorite($data)
    {
        $sql = "SELECT * FROM favorites WHERE iduser = :user AND referencetable = :type AND idelement = :id";
        $req = $this->executerRequete($sql, $data);
        $output = $req->fetch(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * Lecture des Favoris
     * @return array Liste des Favoris de l'utilisateur connecté
     */
    function getFavorites()
    {
        $sql = "SELECT * FROM favorites WHERE iduser = :user";
        $req = $this->executerRequete($sql, array("user" => $_SESSION["user"]));
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * Lecture d'un utilisateur
     * @param string $id ID de l'utilisateur
     * @return array Information concernant un utilisateur
     */
    function getUser($id)
    {
        $sql = "SELECT id, firstname, lastname, mail, picture, registration, administrator FROM users WHERE id = :id";
        $req = $this->executerRequete($sql, array("id" => $id));
        $output = $req->fetch(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * Lecture des utilisateurs
     * @return array Liste des utilisateurs
     */
    function getUsers()
    {
        $sql = "SELECT id, firstname, lastname, mail, picture, registration, administrator FROM users";
        $req = $this->executerRequete($sql);
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * Lecture des planètes
     * @param $idStar int (facultatif) ID de l'étoile correspondante au soleil du système solaire demandé
     * @return array Liste des planètes sans l'article. Si le paramètre $idStar est omis, toutes les planètes seront retournées
     */
    function getPlanets($idStar = null)
    {
        $sql = "SELECT id, idstar, name, stardistance, orbitalperiod, ecliptictilt, radius, rotationperiod, axialtilt
        FROM planets";
        $data = array();
        if ($idStar !== null)
        {
            $sql .= " WHERE idstar = :idStar";
            $data["idStar"] = $idStar;
        }
        $req = $this->executerRequete($sql, $data);
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * Lecture d'une planète
     * @param $id int ID de la planète demandée
     * @return array Informations sur la planète
     */
    function getPlanet($id)
    {
        $sql = "SELECT id, idstar, name, stardistance, orbitalperiod, ecliptictilt, radius, rotationperiod, axialtilt
        FROM planets WHERE id = :id";
        $req = $this->executerRequete($sql, array("id" => $id));
        $output = $req->fetch(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * Lecture des satellites
     * @param $idPlanet int (facultatif) ID de la planète courrante
     * @return array Liste des satellites sans l'article. Si le paramètre $idPlanet est omis, tous les satellites seront retournés
     */
    function getSatellites($idPlanet = null)
    {
        $sql = "SELECT id, idplanet, name, planetdistance, orbitalperiod, ecliptictilt, radius, rotationperiod, axialtilt FROM satellites";
        $data = array();
        if (!empty($idPlanet))
        {
            $sql .= " WHERE idplanet = :idPlanet";
            $data["idPlanet"] = $idPlanet;
        }
        $req = $this->executerRequete($sql, $data);
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * Lecture d'un satellite
     * @param $id int ID du satellite demandé
     * @return array Informations sur le satellite
     */
    function getSatellite($id)
    {
        $sql = "SELECT id, idplanet, name, planetdistance, orbitalperiod, ecliptictilt, radius, rotationperiod, axialtilt
        FROM satellites WHERE id = :id";
        $req = $this->executerRequete($sql, array("id" => $id));
        $output = $req->fetch(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * Lecture de l'étoile du système solaire courrant
     * @param $idStar int ID de l'étoile à utiliser comme soleil
     * @return array Informations concernant le soleil sans l'article
     */
    function getSun($idStar = 0)
    {
        $sql = "SELECT id, radius, tags, hip, hd, hr, gl, bf, name, ra, declination, dist, pmra, pmdec, rv, mag, absmag, spect, ci,
                x, y, z, vx, vy, vz, rarad, decrad, pmrarad, pmdecrad, bayer, flam, con, comp, comp_primary, base, lum, var, var_min, var_max
                FROM stars WHERE hip = :idStar";
        $req = $this->executerRequete($sql, array("idStar" => $idStar));
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * Informations simples sur des étoiles visibles depuis la terre (environ 120 000 étoiles)
     * @param array $data : min, max Interval de sélection par hip (Hipparcos ID)
     * @return array hip, coordonnées x, y, z, distance, ID de la couleur, couleur en hexa
     * @link https://github.com/astronexus/HYG-Database
     * @author Gaël GRZELAK
     */
    function getStars($data)
    {
        $sql = "SELECT stars.hip, stars.x, stars.y, stars.z, stars.dist, stars.idstarcolor, starscolor.id, starscolor.hexa FROM stars
                LEFT JOIN starscolor ON stars.idstarcolor = starscolor.id
                WHERE (name IS NULL OR name != 'Sun') AND stars.hip BETWEEN :min AND :max";
        $req = $this->executerRequete($sql, $data);
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * Informations simples sur une étoile visible depuis la terre
     * @param int $hip Hipparcos ID
     * @return array hip, coordonnées x, y, z, distance, ID de la couleur, couleur en hexa
     * @link https://github.com/astronexus/HYG-Database
     * @author Gaël GRZELAK
     */
    function getStar($hip)
    {
        return $this->getStars(array("min" => $hip, "max" => $hip));
    }

    /**
     * Nombre d'étoiles référencées
     * @return int
     */
    function countStars()
    {
        $sql = "SELECT COUNT(hip) as nbStars FROM stars WHERE name IS NULL OR name != 'Sun'";
        $req = $this->executerRequete($sql);
        $output = (int)$req->fetch(PDO::FETCH_ASSOC)["nbStars"];
        $req->closeCursor();
        return $output;
    }

    /**
     * Récupération d'un utilisateur grâce à son mail
     * @param string $mail
     * @return array Liste les infos de l'utilisateur
     */
    function getUserByMail($mail)
    {
        $sql = "SELECT * FROM users WHERE mail = :mail";
        $req = $this->executerRequete($sql, array("mail" => $mail));
        $output = $req->fetch(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * Récupération d'un article
     * @param string $type : planets, satellites...
     * @param int $id Id de la table correspondante
     * @return array Article concerné
     */
    function getArticle($type, $id)
    {
        $sql = "SELECT article FROM " . $type . " WHERE id = :id";
        $req = $this->executerRequete($sql, array("id" => $id));
        $output = $req->fetch(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output["article"];
    }

    /**
     * Récupération du nom d'un article
     * @param string $type
     * @param int $id
     * @return array
     */
    function getName($type, $id)
    {
        $sql = "SELECT name FROM " . $type . " WHERE id = :id";
        $req = $this->executerRequete($sql, array("id" => $id));
        $output = $req->fetch(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output["name"];
    }

    /**
     * Récupération des tags d'un article
     * @param string $type
     * @param int $id
     * @return array
     */
    function getTags($type, $id)
    {
        $sql = "SELECT tags FROM " . $type . " WHERE id = :id";
        $req = $this->executerRequete($sql, array("id" => $id));
        $output = $req->fetch(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output["tags"];
    }

    /**
     * Mise à jour des tags d'un article
     * @param array $data
     * @return string
     */
    function updateTags($type, $data)
    {
        $sql = "UPDATE " . $type . " SET tags = :tags WHERE id = :id";
        return $this->executerRequete($sql, $data);
    }

    /**
     * Récupération du dictionnaire
     * @param boolean $caseSensitive (facultatif) Pour récupérer les valeurs en minuscules
     * @return array La traduction de différents mots dans différentes langues
     */
    function getDictionary($caseSensitive = false)
    {
        if ($caseSensitive) {
            $sql = "SELECT * FROM dictionary";
        } else {
            $sql = "SELECT id, LOWER(fr) as fr, LOWER(en) as en FROM dictionary";
        }
        $req = $this->executerRequete($sql);
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * Recherche dans chaque entité par nom anglais
     * @param string $name (nom anglais)
     * @return array Tableau contenant les résultats (id seul) pour chaque entité correspondante
     */
    function searchByName($name)
    {
        $output = array();

        // Cherche une planète par nom
        $sql = "SELECT id, name FROM planets WHERE name LIKE :name";
        $req = $this->executerRequete($sql, array("name" => '%'.$name.'%'));
        $output["planets"] = $req->fetchAll(PDO::FETCH_ASSOC);

        // Cherche un satellite par nom
        $sql = "SELECT id, name FROM satellites WHERE name LIKE :name";
        $req = $this->executerRequete($sql, array("name" => '%'.$name.'%'));
        $output["satellites"] = $req->fetchAll(PDO::FETCH_ASSOC);
        /*
        // Cherche une étoile par nom
        $sql = "SELECT id, name FROM stars WHERE name LIKE :name";
        $req = $this->executerRequete($sql, array("name" => '%'.$name.'%'));
        $output["stars"] = $req->fetchAll(PDO::FETCH_ASSOC);
        */
        $req->closeCursor();
        return $output;
    }

    /**
     * Recherche dans chaque entité par tags
     * @param string $tags
     * @return array Tableau contenant les résultats (id seul) pour chaque entité correspondante
     */
    function searchByTags($tag)
    {
        $output = array();

        // Cherche dans les tags des planètes
        $sql = "SELECT id, name FROM planets WHERE tags LIKE :tags";
        $req = $this->executerRequete($sql, array("tags" => '%'.$tag.'%'));
        $output["planets"] = $req->fetchAll(PDO::FETCH_ASSOC);

        // Cherche dans les tags des satellites
        $sql = "SELECT id, name FROM satellites WHERE tags LIKE :tags";
        $req = $this->executerRequete($sql, array("tags" => '%'.$tag.'%'));
        $output["satellites"] = $req->fetchAll(PDO::FETCH_ASSOC);

        $req->closeCursor();
        return $output;
    }

    /**
     * Recherche dans chaque entité par article
     * @param string $search
     * @return array Tableau contenant les résultats (id seul) pour chaque entité correspondante
     */
    function searchInArticle($search)
    {
        $output = array();

        // Cherche dans l'article des planètes
        $sql = "SELECT id, name FROM planets WHERE article LIKE :search";
        $req = $this->executerRequete($sql, array("search" => '%'.$search.'%'));
        $output["planets"] = $req->fetchAll(PDO::FETCH_ASSOC);

        // Cherche dans l'article des satellites
        $sql = "SELECT id, name FROM satellites WHERE article LIKE :search";
        $req = $this->executerRequete($sql, array("search" => '%'.$search.'%'));
        $output["satellites"] = $req->fetchAll(PDO::FETCH_ASSOC);

        $req->closeCursor();
        return $output;
    }

    /**
     * Récupération des constellations
     * @return array Tableau contenant les constellations (sans l'article et les tags)
     */
    function getConstellations()
    {
        $output = array();

        // Cherche dans l'article des planètes
        $sql = "SELECT id, name, con, numberlinks, links FROM constellations";
        $req = $this->executerRequete($sql);
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * Récupération d'une constellation par id
     * @param int $id
     * @return array Tableau contenant les informations de la constellation (sans l'article et les tags)
     */
    function getConstellationById($id)
    {
        $output = array();

        // Cherche dans l'article des planètes
        $sql = "SELECT id, name, con, numberlinks, links FROM constellations WHERE id = :id";
        $req = $this->executerRequete($sql, array("id"=>$id));
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * Récupération d'une constellation par nom
     * @param string $con Nom abrégé de la constellation
     * @return array Tableau contenant les informations de la constellation (sans l'article et les tags)
     */
    function getConstellationByCon($con)
    {
        $output = array();

        // Cherche dans l'article des planètes
        $sql = "SELECT id, name, con, numberlinks, links FROM constellations WHERE con = :con";
        $req = $this->executerRequete($sql, array("con"=>$con));
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

}
