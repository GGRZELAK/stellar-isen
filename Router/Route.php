<?php

namespace Router;

class Route
{

    private $instructionDns;
    private $instructionUrl;
    private $callable;
    private $matches = [];
    private $params = [];

    public function __construct($instructionDns, $instructionUrl, $callable)
    {
        $this->instructionUrl = trim($instructionUrl, '/');
        $this->callable = $callable;
        $this->instructionDns = $instructionDns;
    }

    public function match($currentUrl, $currenrtDns)
    {
        $currentUrl = trim($currentUrl, '/');
        $path = preg_replace_callback('#:([\w]+)#', [$this, 'paramMatch'], $this->instructionUrl);
        $regex = "#^$path$#i";
        $matches = null;
        if (!preg_match($regex, $currentUrl, $matches)) return false;
        if ($currenrtDns != $this->instructionDns && $this->instructionDns != '*') return false;
        array_shift($matches);
        $this->matches = $matches;
        return true;
    }

    private function paramMatch($match)
    {
        if (isset($this->params[$match[1]])) {
            return '(' . $this->params[$match[1]] . ')';
        }
        return '([^/]+)';
    }

    public function call()
    {
        if (is_string($this->callable)) {
            $params = explode('#', $this->callable);
            $controller = "Controller\\" . $params[0] . "Controller";
            $controller = new $controller();
            return call_user_func_array([$controller, $params[1]], $this->matches);
        } else {
            return call_user_func_array($this->callable, $this->matches);
        }
    }

    public function with($param, $regex)
    {
        $this->params[$param] = str_replace('(', '(?:', $regex);
        return $this;
    }

    public function getUrl($params)
    {
        $instructionUrl = $this->instructionUrl;
        foreach ($params as $k => $v) {
            $instructionUrl = str_replace(":$k", $v, $instructionUrl);
        }
        return $instructionUrl;
    }

}
