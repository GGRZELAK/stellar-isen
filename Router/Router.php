<?php

namespace Router;

use Exception;

class Router
{

    private $currentDns;
    private $currentUrl;
    private $routes = [];
    private $namedRoutes = [];

    public function __construct($url)
    {
        $this->currentUrl = $url;
        $this->currentDns = $GLOBALS["subDns"];
    }

    public function get($instructionDns, $instructionUrl, $callable, $name = null)
    {
        return $this->add($instructionDns, $instructionUrl, $callable, $name, 'GET');
    }

    public function post($instructionDns, $instructionUrl, $callable, $name = null)
    {
        return $this->add($instructionDns, $instructionUrl, $callable, $name, 'POST');
    }

    public function add($instructionDns, $instructionUrl, $callable, $name = null, $method = null)
    {
        $route = new Route($instructionDns, $instructionUrl, $callable);
        if ($method) {
            $this->routes[$method][] = $route;
        } else {
            $this->routes[] = $route;
        }
        if (is_string($callable) && $name === null) {
            $name = $callable;
        }
        if ($name) {
            $this->namedRoutes[$name] = $route;
        }
        return $route;
    }

    public function notFound()
    {
        $controller = "Controller\\NotFoundController";
        new $controller();
    }

    public function run()
    {
        foreach ($this->routes as $route) {
            if ($route->match($this->currentUrl, $this->currentDns)) {
                return $route->call();
            }
        }

        if (file_exists("Framework/prod.ini")) $this->notFound();
        else throw new Exception("No matching routes");

        return false;
    }

    public function url($name, $params = [])
    {
        if (!isset($this->namedRoutes[$name])) {
            throw new Exception('No route matches this name');
        }
        return $this->namedRoutes[$name]->getUrl($params);
    }
}
