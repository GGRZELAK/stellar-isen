<?php
namespace Router;

use Framework\App;

require "vendor/autoload.php";

$conf = (file_exists("Framework/prod.ini")) ?  parse_ini_file("Framework/prod.ini", true) : parse_ini_file("Framework/dev.ini", true);

$httpHost = explode('.', $_SERVER["HTTP_HOST"]);
$numbSubDns = count($httpHost);
$dns = array();
$subDns = array();
for($count=0; $count<$numbSubDns; $count++)
{
    if ($count < $numbSubDns-$conf["APP"]["dnsRange"])
    {
        array_push($subDns, $httpHost[$count]);
    } else {
        array_push($dns, $httpHost[$count]);
    }
}

$GLOBALS["subDns"] = (!empty($subDns)) ? implode('.', $subDns) : null;
$GLOBALS["dns"] = implode('.', $dns);

$app = new App();

session_set_cookie_params(0, '/', '.' . $app->dns);
session_start();

// Langage
$GLOBALS["language"] = (!empty($_SESSION["language"])) ? $_SESSION["language"] : "en";

// Activation du cache sauf pour l'API
if ($GLOBALS["subDns"] != "api" && $GLOBALS["subDns"] != "cron")
    header("Cache-Control: max-age=31536000");

$url = (! empty($_GET["url"])) ? strip_tags($_GET["url"]) : null;

$router = new Router($url);

$router->add(null, "/script", "Script#index");

$router->add(null, '/', "Index#index");

$router->add(null, '/user/favorites', "User#favorites");

$router->add(null, '/search', "Search#index");

$router->add(null, '/menu/navigation', "Menu#navigation");

$router->add(null, '/authenticate/', "Authenticate#index");
$router->add(null, '/authenticate/create', "Authenticate#create");
$router->add(null, '/authenticate/remove', "Authenticate#remove");
$router->add(null, '/authenticate/update', "Authenticate#update");
$router->add(null, '/authenticate/disconnect', "Authenticate#disconnect");

$router->add("api", '/', "Api#index");
$router->add("api", '/planets', "Api#planets");
$router->add("api", '/satellites', "Api#satellites");
$router->add("api", '/sun', "Api#sun");
$router->add("api", '/stars', "Api#stars");
$router->add("api", '/article', "Api#article");
$router->add("api", '/constellations', "Api#constellations");
$router->add("api", '/tags', "Api#tags");

$router->run();