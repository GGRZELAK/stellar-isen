/**
 * Gestion des données de l'univers 3D
 */

// Avant main.js

var camera, renderer;

var changeTime = 0;

var currentPopup = "connect";

var firstLoadNavigation = true;

var navigation = true;
var settings = true;

var freeNavig = 0;
var constellations = 1;

var scaleDistance = 1;
var scaleDistanceInstruction = 1;
var scaleDistanceChange = 0;

var scaleTime = 1;
var scaleTimeInstruction = 1;
var scaleTimeChange = 0;

var startSearch, search, mustSearch;
var mustCloseSearch = 0;

// main.js

var dataPlanets, dataSatelittes, dataSatellitesPlanets;

var dataSun;

var nbStars;
var dataStars = [];

var dataConstellations = [];

// initUniverse.js

var container, scene, controls;

var near = 1e-6;
var far = 1e27;

var sun;
var sunHalo;
var sunBigHalo;
var sunInsideCorona;
var sunOutsideCorona, sunOutsideCoronaReference;
var sunFusion;

var pointLight;

var currentObject = instructionObject = "Earth";
var currentType = instructionType = "planets";

var currentData, instructionData;

var jupiterCloud;

// animateUniverse.js

var startTime = Date.now() * 0.001;
var prevChrono = 0;

var cameraDistance;

var sunHaloSize;
var sunBigHaloSize, sunRedBigHalo, sunGreenBigHalo, sunBlueBigHalo;
var sunOutsideCoronaSize;
var sunOpacity, sunRedFusion, sunGreenFusion, sunBlueFusion;

var move = 0;

var objectCurrX = objectCurrY = objectCurrZ = 0;
var objectInstructionX = objectInstructionY = objectInstructionZ = 0;

var deltaX = deltaXs = deltaY = deltaYs = deltaZ = deltaZs = 0;

var vitesseCameraX, vitesseCameraY, vitesseCameraZ = 0;

var articleOpen = 0;
var articleMustOpen = 0;
var articleMustClose = 0;
var articleAnimation = 0;

