/**
 * Initialisation de l'univers 3D
 * 
 */

function initUniverse()
{
	// Intégration dans le DOM

	container = document.getElementById( "container3Drendering" );
	containerWidth = container.offsetWidth;
	containerHeight = container.offsetHeight;
	document.body.appendChild( container );

	// scène

	scene = new THREE.Scene();
	
	// Désactivation du Loader

	$("#pageLoader").css("display", "none");

	// Caméra

	camera = new THREE.PerspectiveCamera( 50, containerWidth / containerHeight, near, far );

	if (currentObject == "Sun") camera.position.x = dataSun["Sun"]["radius"] + dataSun["Sun"]["radius"] * 4;
	else if (currentObject == "Milky Way") camera.position.z = far;
	else if (currentType == "planets")
	{
		camera.position.z = dataSun["Sun"]["radius"] + dataPlanets[currentObject]["stardistance"] + dataPlanets[currentObject]["radius"];
		camera.position.x = dataPlanets[currentObject]["radius"] * 3;
	}

	// Controles

	controls = new THREE.OrbitControls( camera, container );

	if (currentObject == "Sun") controls.target.z = 0;
	else if (currentObject == "Milky Way") controls.target.z = 0;
	else if (currentType == "planets")
		controls.target.z = dataSun["Sun"]["radius"] + dataPlanets[currentObject]["stardistance"] + dataPlanets[currentObject]["radius"];

	// Rendu

	renderer = new THREE.WebGLRenderer( { antialias: true, logarithmicDepthBuffer: true } );
	//renderer.shadowMap.enabled = true;
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( containerWidth, containerHeight );
	//renderer.sortObjects = false;
	container.appendChild( renderer.domElement );

	// Galaxy
	
	// Milky Way
	/*
	var geometry = new THREE.BoxGeometry( 1e18, 1e18, 1e18 );
	var loader = new THREE.CubeTextureLoader();
	loader.setPath( "/Assets/img/textures/milky_way/" );

	var textureCube = loader.load( [
		'fx.jpg', 'nx.jpg',
		'fy.jpg', 'ny.jpg',
		'fz.jpg', 'nz.jpg'
	] );

	var material = new THREE.MeshBasicMaterial( { envMap: textureCube, side: THREE.DoubleSide, transparent: true, opacity: 0.5 } );

	var cube = new THREE.Mesh( geometry, material );
	scene.add( cube );
	 */
	
	// Soleil et Lumières

	// Grand halo

	var sunBigHaloTexture = new THREE.TextureLoader().load( "/Assets/img/textures/sun/big_halo.png" );
	var sunBigHaloMaterial = new THREE.SpriteMaterial( { map: sunBigHaloTexture } );
	sunBigHalo = new THREE.Sprite( sunBigHaloMaterial );

	scene.add( sunBigHalo );

	// Petit halo

	var sunHaloTexture = new THREE.TextureLoader().load( "/Assets/img/textures/sun/halo.png" );
	var sunHaloMaterial = new THREE.SpriteMaterial( { map: sunHaloTexture, color: 0xffffff } );
	sunHalo = new THREE.Sprite( sunHaloMaterial );

	scene.add( sunHalo );

	// Texture

	var sunTexture = new THREE.TextureLoader().load( "/Assets/img/textures/sun/sun.png" );
	var sunGeometry = new THREE.SphereBufferGeometry( dataSun["Sun"]["radius"] , 32, 32 );
	var sunMaterial = new THREE.MeshBasicMaterial( { map: sunTexture, transparent: true } );
	sun = new THREE.Mesh( sunGeometry, sunMaterial );

	// Sphère blanche ou clignotante rouge quand on est proche

	var sunFusionGeometry = new THREE.SphereBufferGeometry( dataSun["Sun"]["radius"] , 32, 32 );
	var sunFusionMaterial = new THREE.MeshBasicMaterial( { color: 0xff0000 } );
	sunFusion = new THREE.Mesh( sunFusionGeometry, sunFusionMaterial );
	sunFusion.name = "Sun";

	scene.add( sunFusion );

	// Couronne extérieure

	var sunOutsideCoronaTexture = new THREE.TextureLoader().load( "/Assets/img/textures/sun/corona.png" );
	var sunOutsideCoronaMaterial = new THREE.SpriteMaterial( { map: sunOutsideCoronaTexture, color: 0xffffff } );
	sunOutsideCorona = new THREE.Sprite( sunOutsideCoronaMaterial );
	sunOutsideCorona.name = "sunOutsideCorona";
	sunOutsideCoronaReference = 2300000;

	// Shaders pour la couronne intérieure

	var sunInsideCoronaGeometry =  new THREE.SphereBufferGeometry( dataSun["Sun"]["radius"], 32, 32 );
	var sunInsideCoronaMaterial = new THREE.ShaderMaterial( {
		uniforms:
		{
			coeficient	:
			{
				type	: 'f',
				value	: 1.0
			},
			power		:
			{
				type	: 'f',
				value	: 1
			},
			glowColor	:
			{
				type	: 'c',
				value	: new THREE.Color( 0xffffff )
			}
		},
		vertexShader	: coronaVertexShader,
		fragmentShader	: coronaInsideFragmentShader,
		blending		: THREE.NormalBlending,
		transparent		: true,
		depthTest		: false
	} );
	sunInsideCorona = new THREE.Mesh( sunInsideCoronaGeometry, sunInsideCoronaMaterial );
	sunInsideCorona.name = "sunInsideCorona";

	// Lumières

	pointLight = new THREE.PointLight( 0xffffff, 1 );
	//pointLight.castShadow = true;

	pointLight.add( sunFusion );

	scene.add( pointLight );

	// Lumière ambiante

	var ambientLight = new THREE.AmbientLight( 0xffffff, 0.25 );
	scene.add( ambientLight );

	// Réglages des ombres
/*
	pointLight.shadow.mapSize.width = 1920;
	pointLight.shadow.mapSize.height = 1080;
	pointLight.shadow.camera.near = near;
	pointLight.shadow.camera.far = far;
*/

	for ( idPlanet in dataPlanets )
	{
		planet = dataPlanets[idPlanet];
		
		planetTexture = new THREE.TextureLoader().load( "/Assets/img/textures/" + planet["name"] + ".jpg" );

		planetGeometry = new THREE.SphereBufferGeometry( planet["radius"], 120, 60 );
		planetMaterial = new THREE.MeshLambertMaterial( { map: planetTexture } );
		planetObject = new THREE.Mesh( planetGeometry, planetMaterial );

		planetObject.name = planet["name"];

		planetObject.rotation.x = degreeToRadian( planet["axialtilt"] );

		planetObjectSystem = new THREE.Group();

		planetObjectSystem.name = planet["name"] + "System";

		planetObjectSystem.position.z = dataSun["Sun"]["radius"] + planet["stardistance"] + planet["radius"];
		
		planetObjectSystem.add( planetObject );
		
		if ( typeof dataSatellitesPlanets[planet["id"]] != "undefined" )
		{
			for ( idSatellite in dataSatellitesPlanets[planet["id"]] )
			{
				satellite = dataSatellitesPlanets[planet["id"]][idSatellite];
				
				satelliteTexture = new THREE.TextureLoader().load( "/Assets/img/textures/" + satellite["name"] + ".jpg" );
	
				satelliteGeometry = new THREE.SphereBufferGeometry( satellite["radius"], 64, 32 );
				satelliteMaterial = new THREE.MeshLambertMaterial( { map: satelliteTexture } );
				satelliteObject = new THREE.Mesh( satelliteGeometry, satelliteMaterial );
				
				satelliteObject.name = satellite["name"];
				
				satelliteObject.position.z = planet["radius"] + satellite["planetdistance"] + satellite["radius"];
				
				satelliteObject.rotation.x = degreeToRadian(satellite["axialtilt"]);
				
				satelliteObjectSystem = new THREE.Group();
	
				satelliteObjectSystem.name = satellite["name"] + "System";
	
				satelliteObjectSystem.rotation.x = degreeToRadian(satellite["ecliptictilt"]);
				
				satelliteObjectSystem.add( satelliteObject );
				
				planetObjectSystem.add( satelliteObjectSystem );
			}
		}
		
		scene.add( planetObjectSystem );
	}

	// Effets sur la terre
	
	// Shaders pour les couronnes intérieure et extérieure de la terre

	var earthInsideCoronaGeometry =  new THREE.SphereBufferGeometry( dataPlanets["Earth"]["radius"]+50, 32, 32 );
	var earthOutsideCoronaGeometry =  new THREE.SphereBufferGeometry( dataPlanets["Earth"]["radius"]-50 + 400, 32, 32 );

	var earthInsideCoronaMaterial	= new THREE.ShaderMaterial( {
		uniforms:
		{
			coeficient	:
			{
				type	: 'f',
				value	: 1.0
			},
			power		:
			{
				type	: 'f',
				value	: 2
			},
			glowColor	:
			{
				type	: 'c',
				value	: new THREE.Color( "rgba(2%, 45%, 85%)" )
			}
		},
		vertexShader	: coronaVertexShader,
		fragmentShader	: coronaInsideFragmentShader,
		blending		: THREE.NormalBlending,
		transparent		: true,
		depthTest		: false
	} );

	var earthOutsideCoronaMaterial	= new THREE.ShaderMaterial( {
		uniforms:
		{
			coeficient	:
			{
				type	: 'f',
				value	: 0.2
			},
			power		:
			{
				type	: 'f',
				value	: 2
			},
			glowColor	:
			{
				type	: 'c',
				value	: new THREE.Color("rgb(15%, 55%, 90%)")
			}
		},
		vertexShader	: coronaVertexShader,
		fragmentShader	: coronaOutsideFragmentShader,
		blending		: THREE.NormalBlending,
		transparent		: true
	} );

	var earthInsideCorona = new THREE.Mesh( earthInsideCoronaGeometry, earthInsideCoronaMaterial );
	var earthOutsideCorona = new THREE.Mesh( earthOutsideCoronaGeometry, earthOutsideCoronaMaterial );
	
	scene.getObjectByName( "EarthSystem" ).add( earthOutsideCorona );
	scene.getObjectByName( "EarthSystem" ).add( earthInsideCorona );

	// ISS
	/*
		var GLTFLoader = new THREE.GLTFLoader();

		GLTFLoader.load( '/Assets/obj/international_space_station/scene.gltf', function ( gltf ) {

			( gltf.scene ).position.z = - dataPlanets["Earth"]["radius"] - 400;
			( gltf.scene ).rotation.x = degreeToRadian(90);
			( gltf.scene ).rotation.y = degreeToRadian(90);
			( gltf.scene ).scale.x = 0.01;
			( gltf.scene ).scale.y = 0.01;
			( gltf.scene ).scale.z = 0.01;

			earthSystem.add( gltf.scene );
		} );
	 */
	
	// Effets sur Jupiter

	var jupiterCloudTexture = new THREE.TextureLoader().load( "/Assets/img/textures/jupiter_cloud.png" );

	var jupiterCloudGeometry = new THREE.SphereBufferGeometry( dataPlanets["Jupiter"]["radius"] + 100, 64, 64 );
	var jupiterCloudMaterial = new THREE.MeshLambertMaterial( { map: jupiterCloudTexture, transparent: true } );
	jupiterCloud = new THREE.Mesh( jupiterCloudGeometry, jupiterCloudMaterial );
	
	// Inclinaison des nuages
	
	jupiterCloud.rotation.x = degreeToRadian(dataPlanets["Jupiter"]["axialtilt"]);
	
	scene.getObjectByName( "JupiterSystem" ).add( jupiterCloud );
	
	// Effets sur Saturne

	var saturnRingTexture = new THREE.TextureLoader().load( "/Assets/img/textures/saturn_ring.png" );

	var saturnRingInnerRadius = dataPlanets["Saturn"]["radius"] + 7000;
	var saturnRingOuterRadius = dataPlanets["Saturn"]["radius"] + 72000;

	var saturnRingGeometry = new THREE.XRingGeometry( saturnRingInnerRadius, saturnRingOuterRadius, 128, 5 );
	var saturnRingMaterial = new THREE.MeshBasicMaterial( { map: saturnRingTexture, transparent: true, side: THREE.DoubleSide } );
	var saturnRing = new THREE.Mesh( saturnRingGeometry, saturnRingMaterial );
	
	saturnRing.rotation.x = degreeToRadian( dataPlanets["Saturn"]["axialtilt"] );
	
	scene.getObjectByName( "SaturnSystem" ).add( saturnRing );
	
	// Effets sur Uranus

	var uranusRingTexture = new THREE.TextureLoader().load( "/Assets/img/textures/uranus_ring.png" );

	var uranusRingInnerRadius = 38000;
	var uranusRingOuterRadius = 98000;

	var uranusRingGeometry = new THREE.XRingGeometry( uranusRingInnerRadius, uranusRingOuterRadius, 128, 5 );
	var uranusRingMaterial = new THREE.MeshBasicMaterial( { map: uranusRingTexture, transparent: true, side: THREE.DoubleSide, opacity: 0.25 } );
	var uranusRing = new THREE.Mesh( uranusRingGeometry, uranusRingMaterial );
	
	uranusRing.rotation.x = degreeToRadian( dataPlanets["Uranus"]["axialtilt"] );
	
	scene.getObjectByName( "UranusSystem" ).add( uranusRing );
	
	objectCurrZ = objectInstructionZ = scene.getObjectByName( instructionObject ).position.z;
}
