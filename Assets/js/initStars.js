/**
 * Initialisation des étoiles
 * @param data array Données des étoiles à rajouter
 * @link https://github.com/astronexus/HYG-Database
 */

function initStars(data)
{
	uniforms = { texture: { value: new THREE.TextureLoader().load( "Assets/img/textures/star.png" ) } };

	var shaderMaterial = new THREE.ShaderMaterial( {
		uniforms: uniforms,
		vertexShader: starVertexShader,
		fragmentShader: starFragmentShader,
		blending: THREE.AdditiveBlending,
		//depthTest: false,
		transparent: true,
		vertexColors: true
	} );

	geometry = new THREE.BufferGeometry();
	var positions = [];
	var colors = [];
	var sizes = [];
	var color = new THREE.Color();

	for( element in data ) {

		// "To convert parsecs to light years, multiply by 3.262"
		// 1 année lumière = 9.461e12 km
		parsecsToKm = 3.262 * 9.461e12;

		positions.push( data[element].x * parsecsToKm );
		positions.push( data[element].y * parsecsToKm );

		//centrage des étoiles sur la Terre
		positions.push( data[element].z * parsecsToKm + dataSun["Sun"]["radius"] + dataPlanets["Earth"]["stardistance"] + dataPlanets["Earth"]["radius"] );

		color.setHex("0x"+data[element].hexa);
		colors.push( color.r, color.g, color.b );

		sizes.push( parsecsToKm / 3);
	}

	geometry.addAttribute( 'position', new THREE.Float32BufferAttribute( positions, 3 ) );
	geometry.addAttribute( 'color', new THREE.Float32BufferAttribute( colors, 3 ) );
	geometry.addAttribute( 'size', new THREE.Float32BufferAttribute( sizes, 1 ).setDynamic( true ) );

	particleSystem = new THREE.Points( geometry, shaderMaterial );

	scene.add( particleSystem );
}
