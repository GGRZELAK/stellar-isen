/**
 * Initialisation des constellations
 * @link https://github.com/Stellarium/stellarium/blob/master/skycultures/western/constellationship.fab
 */
function initConstellations()
{
	// Récupération des données
	$.ajax(
	{
		type : "GET",
		dataType : "json",
		url: protocol + "api." + dns + "/constellations",
		timeout: 3000,
		xhrFields: {
			withCredentials: true
		},
		success : function(response)
		{
			dataConstellations = array_column(response, null, "con");
			parsecsToKm = 3.262 * 9.461e12;
			// Affichage

			var loader = new THREE.FontLoader();
			loader.load( '/Assets/fonts/Eczar/Eczar_Regular.json', function ( font )
			{
				for (constellation in dataConstellations)
				{
					xSum = 0;
					ySum = 0;
					zSum = 0;
					starsConstellation = [];
					
					link = 0;

					links = dataConstellations[constellation]["links"].split(' ');

					for (couple = 0; couple < dataConstellations[constellation]['numberlinks']; couple++)
					{
						link = couple * 2;
						
						if (links[link] != undefined)
							starOne = parseInt(links[link]);
						
						if (links[link+1] != undefined)
							starTwo = parseInt(links[link+1]);

						if (dataStars[starOne] != undefined && dataStars[starTwo] != undefined)
						{
							var constellationMaterial = new THREE.LineBasicMaterial({color: 0xffffff, opacity: 0.25, transparent: true });
							var constellationGeometry = new THREE.Geometry();
							constellationGeometry.vertices.push(
									new THREE.Vector3(
											dataStars[starOne]['x'] * parsecsToKm,
											dataStars[starOne]['y'] * parsecsToKm,
											dataStars[starOne]['z'] * parsecsToKm
									),
									new THREE.Vector3(
											dataStars[starTwo]['x'] * parsecsToKm,
											dataStars[starTwo]['y'] * parsecsToKm,
											dataStars[starTwo]['z'] * parsecsToKm
									));
							var line = new THREE.Line( constellationGeometry, constellationMaterial );
							
							line.name = "line"+couple+dataConstellations[constellation]['name'];
							
							scene.add( line );
	
							if (!starsConstellation.includes(starOne))
							{
								xSum += dataStars[starOne]['x'] * parsecsToKm;
								ySum += dataStars[starOne]['y'] * parsecsToKm;
								zSum += dataStars[starOne]['z'] * parsecsToKm;
	
								starsConstellation.push(starOne);
							}
							if (!starsConstellation.includes(starTwo))
							{
								xSum += dataStars[starTwo]['x'] * parsecsToKm;
								ySum += dataStars[starTwo]['y'] * parsecsToKm;
								zSum += dataStars[starTwo]['z'] * parsecsToKm;
	
								starsConstellation.push(starTwo);
							}
						}
					}

					xCentroid = xSum / starsConstellation.length;
					yCentroid = ySum / starsConstellation.length;
					zCentroid = zSum / starsConstellation.length;
					constellationDistance = Math.sqrt( Math.pow( xCentroid, 2 ) + Math.pow( yCentroid, 2 ) + Math.pow( zCentroid, 2 ) );

					var geometry = new THREE.TextBufferGeometry( dataConstellations[constellation]['name'], {
						font: font,
						size: constellationDistance/75,
						height: 0,
						curveSegments: 12,
						bevelEnabled: false
					} );

					geometry.computeBoundingBox();
					xMid = - 0.5 * (geometry.boundingBox.max.x - geometry.boundingBox.min.x);
					geometry.translate(xMid, 0, 0);
					var material = new THREE.MeshLambertMaterial( {color: 0xffffff} );
					var text = new THREE.Mesh( geometry, material );
					
					text.position.x = xCentroid;
					text.position.y = yCentroid;
					text.position.z = zCentroid;
					
					text.name = dataConstellations[constellation]['name'];
					
					scene.add( text );

					//text.quaternion.copy(camera.quaternion);
					text.lookAt( camera.position );
				}
			});
			
			$("#constellations").prop("checked", true);
		}
	});
}