// Conversion des angles

function degreeToRadian( degree ) {
	
	return degree * Math.PI / 180;
}

// Redimensionnement du canvas pour un affichage adaptatif

function resize() {
	
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();

	renderer.setSize( window.innerWidth, window.innerHeight );
}

// Ouverture d'une modale (popup)

function openPopup(popup) {
	
	$("#popup").css("display", "flex");
	$("#"+popup).css("display", "block");
}

// Fermeture d'une modale (popup)

function closePopup(popup) {
	
	$("#popup").css("display", "none");
	$("#"+popup).css("display", "none");
}

// Ouverture d'un menu

function openMenu(objectName, position) {
	
	object = $(objectName);
	pos = object.css(position);
	pos = parseInt(pos.replace("px", ''));
	
	if (pos != 0) {
		
		object.css(position, "0px");
		
		switch (objectName) {
			case "nav":
				navigation = true;
				break;
			case "#settings":
				settings = true;
				break;
		}
	}
}

// Fermeture d'un menu

function closeMenu(objectName, position) {
	
	object = $(objectName);
	pos = object.css(position);
	pos = parseInt(pos.replace("px", ''));
	
	if (pos == 0 || pos < -object.innerWidth()) {
		
		object.css(position, -object.innerWidth() + "px");
		
		switch (objectName) {
			case "nav":
				navigation = false;
				break;
			case "#settings":
				settings = false;
				break;
		}
	}
}

// Identification

function identification(response) {
	
	authenticate = JSON.parse(response);
	
	if (typeof authenticate != undefined) {
		
		if (authenticate["error"]) {
			
			$.growl.error({ message: authenticate["error"] });
		}
		else if (authenticate["success"]) {
			
			$.growl.notice({ message: "Bienvenue " + authenticate["success"]["firstname"] + ' ' + authenticate["success"]["lastname"] });
			
			// Fermeture de la modale de connexion
			
			closePopup("profil");
			
			// Cacher la modale d'enregistrement
			
			$("#register").css("display", "none");
			
			// Transformation de la modale d'identification en modale de Mise à jour
			
			$("#profil h3").html("Profil");
			$('#profil input[type="email"]').val(authenticate["success"]["mail"]);
			$("#profil #registerButton").css("display", "none");
			$("#profil #disconnectButton").css("display", "block");
			$('#profil button[type="submit"]').html("Mettre à jour");
			
			// Affichage des données utilisateur
			
			$("#settings #user #button-connect").attr("id", "button-profil");
			
			image = null;
			
			if (authenticate["success"]["picture"]) {
                
                image = '<img src="' + authenticate["success"]["picture"] + '" alt="Poto de profil" />';
            
            } else {
                
                image = '<i class="material-icons">account_circle</i>';
            }
			
			$("#settings #user #button-profil").html(
					image+
					'<span class="name">'+
					authenticate["success"]["firstname"]+
					"<br />"+
					authenticate["success"]["lastname"]+
					"</span>"
			);
			
			reloadNavigation();
		}
	} else {
		
		$.growl.error({ message: "Stellar'isen ne parvient pas à communiquer avec le serveur." });
	}
}

// Déconnexion

function deconnect(that) {

    $.ajax({
    	
		type : 'POST',
        data : $(that).serialize(),
		url : '/authenticate/disconnect',
		timeout: 3000,
		xhrFields: {
			withCredentials: true
		},
		success : function(response) {
			
			$.growl.notice({ message: "Vous avez été déconnecté." });
			
			// Fermeture de la modale de connexion
			
			closePopup("profil");
			
			// Transformation de la modale d'identification en modale de Mise à jour
			
			$("#profil h3").html("S'identifier");
			$("#profil #registerButton").css("display", "block");
			$("#profil #disconnectButton").css("display", "none");
			$('#profil button[type="submit"]').html("Se connecter");
			
			// Affichage des données utilisateur
			
			$("#settings #user #button-profil").attr("id", "button-connect");
			$("#settings #user #button-connect").html("Connexion");
			
			reloadNavigation();
		},
		error: function() {

			$.growl.error({ message: "Vous n'êtes pas déconnecté. Stellar'isen ne parvient pas à communiquer avec le serveur." });
		}
    });
}

// Mise à jour d'un utilisateur

function updateUser(that) {
	
	$.ajax({
		
		type : 'POST',
	    data : $(that).serialize(),
		url : '/authenticate/create',
		timeout: 3000,
		xhrFields: {
			withCredentials: true
		},
		success : function(response) {
			
			identification(response);
		},
		error: function() {

			$.growl.error({ message: "Stellar'isen ne parvient pas à communiquer avec le serveur." });
		}
	});
}

// Connexion ou Mise à jour de l'utilisateur (POST modale #profil)

function postProfil(that) {
	
	if (currentPopup == "connect") {
		
	    $.ajax({
	    	
			type : 'POST',
	        data : $(that).serialize(),
			url : '/authenticate',
			timeout: 3000,
			xhrFields: {
				withCredentials: true
			},
			success : function(response) {
				
				identification(response);
			},
			error: function() {

				$.growl.error({ message: "Stellar'isen ne parvient pas à communiquer avec le serveur." });
			}
	    });
	}
	else if (currentPopup == "profil") {
		
	    $.ajax({
	    	
			type : 'POST',
	        data : $(that).serialize(),
			url : '/authenticate/update',
			timeout: 3000,
			xhrFields: {
				withCredentials: true
			},
			success : function(response) {
				
				authenticate = JSON.parse(response);
				
				if (typeof authenticate != undefined) {
					
					if (authenticate["error"]) {
						
						$.growl.error({ message: authenticate["error"] });
					}
					else if (authenticate["success"]) {
						
						$.growl.notice({ message: authenticate["success"] });
					}
				}
			},
			error: function() {

				$.growl.error({ message: "Stellar'isen ne parvient pas à communiquer avec le serveur." });
			}
	    });
	}
}

// Change l'objet courrant

function changeObject(hash, time) {
	
	instructionObject = hash;
	changeTime = time;
	move = 1;
}

// Track le changement d'URL

function locationHashChanged() {
	
	hash = location.hash.substring(1);
	chrono = 0;
	
	if (dataSun[hash] != undefined) {
		
		instructionData = dataSun;
		instructionType = "stars";
		
		changeObject(hash, chrono);
	}
	else if (dataPlanets[hash] != undefined) {
		
		instructionData = dataPlanets;
		instructionType = "planets";
		
		changeObject(hash, chrono);
	}
	else if (dataSatellites[hash] != undefined) {
		
		instructionData = dataSatellites;
		instructionType = "satellites";
		
		changeObject(hash, chrono);
	}
}

// Enregistre et affiche les nouveaux tags

function setTags(that, type, id) {

	data = $(that).serialize();
	
	$.ajax({
	
		type : 'POST',
		data : data + "&type=" + type + "&id=" + id,
		url : protocol + "api." + dns + "/tags",
		timeout: 3000,
		xhrFields: {
			withCredentials: true
		},
		success : function(response) {
	
			$("#updateTags").html(response);
		},
		error: function() {

			$.growl.error({ message: "Stellar'isen ne parvient pas à communiquer avec le serveur." });
		}
	});
}

// Ajouter ou supprimer des favoris

function setFavorites(type, id) {
	
	$.ajax({
		
		type : 'POST',
		data : "type=" + type + "&id=" + id,
		url : '/user/favorites',
		timeout: 3000,
		xhrFields: {
			withCredentials: true
		},
		success : function(response) {
	
			if ($("#changeFavorite i").html() == "favorite_border") {
				
				$("#changeFavorite i").html("favorite");
				$("#changeFavorite").attr("title", "Ajouter aux favoris");
			} else {
				
				$("#changeFavorite i").html("favorite_border");
				$("#changeFavorite").attr("title", "Supprimer des favoris");
			}
	
			reloadNavigation();
		},
		error: function() {

			$.growl.error({ message: "Stellar'isen ne parvient pas à communiquer avec le serveur." });
		}
	});
}

// Convertit un article HTML en PDF

function generatePDF() {
	
	var specialElementHandlers = 
		function (element,renderer) {
		return true;
	}
	var doc = new jsPDF();
	doc.fromHTML($('.infobox_v2').html(), 0, 0, {}, function(){doc.save('file.pdf');});
}

// Récupère l'article à afficher

function displayArticle(type, id)
{
	$.ajax({

		type : "GET",
		dataType : "html",
		url: protocol + "api." + dns + "/article?type=" + type + "&id=" + id,
		timeout: 3000,
		xhrFields: {
			withCredentials: true
		},
		success : function( response, statut ) {
			
			$("#article > .article-body").html(response);

			$("#article").css("display", "block");
			
			articleEvents(type, id);
		},
		error: function() {

			$.growl.error({ message: "Stellar'isen ne parvient pas à communiquer avec le serveur." });
		}
	});
}

// Recharge le menu de navigation

function reloadNavigation()
{
	$.ajax({
 	
		type : 'GET',
		dataType : "html",
		url : '/menu/navigation?type=' + currentType + '&id=' + currentData[currentObject]["id"],
		timeout: 3000,
		xhrFields: {
			withCredentials: true
		},
		success : function(response) {
			
			$("nav > .navigationContainer > .menu").html(response);
			
			if (firstLoadNavigation) {

				closeMenu("nav", "left");
				firstLoadNavigation = false;
			}
			
			showArticleEvent();
		},
		error: function() {

			$.growl.error({ message: "Stellar'isen ne parvient pas à communiquer avec le serveur." });
		}
	});
}

// Demande l'ouverture de l'article

function showArticle() {

	if (!articleOpen)
	{
		articleMustOpen = 1;
		
		// Selon la résolution, on désactive le décalage de l'astre mais on active les boutons de scroll
		
		if (window.matchMedia("(min-width:1000px)").matches) {
			
			articleAnimation = 1;
			
		} else {
			
			$("#seeArticle .scrollTo").css("display", "flex");
		}
	}
}

// Effectuer une recherche

function doSomeSearch() {
	
	$.ajax({
	
		type : 'GET',
		url : '/search?name='+search+"&tag="+search+"&article="+search,
		timeout: 3000,
		xhrFields: {
			withCredentials: true
		},
		success : function(response) {
	
			$("#searchLoader").css("display", "none");
			$("#searchResults").css("display", "block");
			$("#menu").css("display", "none");
			$("#searchResults").html(response);
	
			showArticleEvent();
		},
		error: function() {

			$.growl.error({ message: "Stellar'isen ne parvient pas à communiquer avec le serveur." });
		}
	});
	
	mustSearch = 0;
	mustCloseSearch = 1;
}

// Suppression des constellations

function removeConstellations() {

	for (constellation in dataConstellations)
	{
		for (couple = 0; couple < dataConstellations[constellation]['numberlinks']; couple++)
		{
			if ( coupleLine = scene.getObjectByName( "line"+couple+dataConstellations[constellation]['name'] ) )
			{
				coupleLine.name = null;
				scene.remove( coupleLine );
			}
		}
		
		if ( constellationObject = scene.getObjectByName( dataConstellations[constellation]['name'] ) )
		{
			constellationObject.name = null;
			scene.remove( constellationObject );
		}
	}
}

/**
 * array_column() retourne les valeurs d'une colonne de input, identifiée par la clé column_key.
 * Optionnellement, vous pouvez fournir un paramètre index_key pour indexer les valeurs dans le tableau retourné par les valeurs de la colonne index_key du tableau d'entrée.
 * @param input Un tableau multi-dimensionnel ou un tableau d'objets à partir duquel on extrait une colonne de valeur.
 * @param column_key La colonne de valeurs à retourner. Cette valeur peut être la clé entière de la colonne que vous souhaitez récupérer, ou bien le nom de la clé pour un tableau associatif. Il peut aussi valoir NULL pour retourner le tableau complet ou des objets (ceci peut être utile en conjonction du paramètre index_key pour ré-indexer le tableau).
 * @param index_key La colonne à utiliser comme index/clé pour le tableau retourné. Cette valeur peut être la clé entière de la colonne, ou le nom de la clé.
 * @return Retourne un tableau de valeurs représentant une seule colonne depuis le tableau d'entrée.
 * @link http://php.net/manual/fr/function.array-column.php Pour voir des exemples d'utilisations
 * @author Gaël GRZELAK
 */
function array_column(input, column_key = null, index_key = false) {
	
	array = {};
	
	for (firstKey in input) {
		
		if (index_key && input[firstKey][index_key] != "undefined") {
			
			key = input[firstKey][index_key];
			
		} else {
			
			key = firstKey;
		}

		if (column_key && input[firstKey][column_key] != "undefined") {
			
			array[key] = input[firstKey][column_key];
		}
		else if (input[firstKey] != "undefined") {
			
			array[key] = input[firstKey];
		}
	}
	
	return array;
}

// Lancement du rendu 3D

function init() {
	
	initUniverse();
	animateUniverse();

	//On check l'URL dès le premier chargement de la page

	locationHashChanged();
}

// Récupère le reste des étoiles
// Requêtes par salve de 5000 étoiles pour récupérer la totalité des données

function loadStars(limit, min, max) {
	
	max = min + limit - 1;
	if (max > nbStars) max = nbStars;

	$.ajax({

		type : "GET",
		dataType : "json",
		url: protocol + "api." + dns + "/stars?min="+min+"&max="+max,
		timeout: 3000,
		xhrFields: {
			withCredentials: true
		},
		success : function(response) {
			
			data = array_column(response, null, "hip");
			dataStars = Object.assign(dataStars, data);
			initStars(data);
			min += limit;
			if (max < nbStars) loadStars(limit, min, max);
			else initConstellations();
		},
		error : function() {
	
			$.growl.error({ message: "Erreur de chargement des étoiles. Veuillez recharger la page." });
		}
	});
}

// Récupère le nombre d'étoiles disponibles en BDD

function loadNbStars() {
	
	$.ajax({
	
		type : "GET",
		dataType : "text",
		url: protocol + "api." + dns + "/stars",
		timeout: 3000,
		xhrFields: {
			withCredentials: true
		},
		success : function(response) {
			
			nbStars = response;
			
			init();
			loadStars(5000, 1, 0);
		},
		error : function() {
	
			$.growl.error({ message: "Erreur de chargement des étoiles. Veuillez recharger la page." });
		}
	});
}

// Récupère le soleil

function loadSun() {
	
	$.ajax({
	
		type : "GET",
		dataType : "json",
		url: protocol + "api." + dns + "/sun",
		timeout: 3000,
		xhrFields: {
			withCredentials: true
		},
		success : function(response) {
			
			dataSun = array_column(response, null, "name");
			
			loadNbStars();
		},
		error : function() {
	
			$.growl.error({ message: "Erreur de chargement du soleil. Veuillez recharger la page." });
		}
	});
}

// Récupère les satellites

function loadSatellites() {

	$.ajax({
	
		type : "GET",
		dataType : "json",
		url: protocol + "api." + dns + "/satellites",
		timeout: 3000,
		xhrFields: {
			withCredentials: true
		},
		success : function(response) {
			
			dataSatellitesPlanets = new Array();
			
			for (id in response) {
				
				satellite = response[id];
				if (typeof(dataSatellitesPlanets[satellite["idplanet"]]) == "undefined") {
					
					dataSatellitesPlanets[satellite["idplanet"]] = new Array();
				}
				dataSatellitesPlanets[satellite["idplanet"]][satellite["name"]] = satellite;
			}
			
			dataSatellites = array_column(response, null, "name");
			
			loadSun();
		},
		error : function() {
	
			$.growl.error({ message: "Erreur de chargement des Satellites naturels. Veuillez recharger la page." });
		}
	});
}

// Récupère les planètes

function loadPlanets() {

	$.ajax({
	
		type : "GET",
		dataType : "json",
		url: protocol + "api." + dns + "/planets",
		timeout: 3000,
		xhrFields: {
			withCredentials: true
		},
		success : function(response) {
			
			dataPlanets = array_column(response, null, "name");
			
			currentData = instructionData = dataPlanets;
			
			// Premier chargement du menu de Navigation

			reloadNavigation();
			
			loadSatellites();
		},
		error : function() {

			$.growl.error({ message: "Erreur de chargement des planètes. Veuillez recharger la page." });
		}
	});
}

// Chargement des données de l'univers

function loadData() {
	
	// Récupère les planètes
	
	loadPlanets();
}

// Après la fin du chargement de la page

$(function() {

	// Chargement des données de l'univers
	
	loadData();
	
});
