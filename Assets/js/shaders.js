/**
 * VertexShader et FragmentShader (Microprogrammes à envoyer à la carte graphique)
 * Calculs mathématiques pour les étoiles et les courrones du soleil et de la terre
 * @author Gaël GRZELAK
 */

var starVertexShader = [
    
	'attribute float size;',
	'varying vec3 vColor;',
	
	'void main() {',
	
		'vColor = color;',
		'vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );',
		'gl_PointSize = size * ( 300.0 / -mvPosition.z );',
		'gl_Position = projectionMatrix * mvPosition;',
	'}'

].join('\n');

var coronaVertexShader = [
    
    'varying vec3	vVertexWorldPosition;',
    'varying vec3	vVertexNormal;',

    'varying vec4	vFragColor;',

    'void main(){',
    
    '	vVertexNormal = normalize( normalMatrix * normal );',

    '	vVertexWorldPosition = ( modelMatrix * vec4( position, 1.0 ) ).xyz;',

    '	// set gl_Position',
    '	gl_Position	= projectionMatrix * modelViewMatrix * vec4( position, 1.0 );',
    '}'

].join('\n');

var starFragmentShader	= [
    
	'uniform sampler2D texture;',
	'varying vec3 vColor;',
	
	'void main() {',
		'gl_FragColor = vec4( vColor, 1.0 );',
		'gl_FragColor = gl_FragColor * texture2D( texture, gl_PointCoord );',
	'}'
].join('\n');

var coronaInsideFragmentShader	= [
    
    'uniform vec3	glowColor;',
    'uniform float	coeficient;',
    'uniform float	power;',

    'varying vec3	vVertexNormal;',
    'varying vec3	vVertexWorldPosition;',

    'varying vec4	vFragColor;',

    'void main() {',
    '	vec3 worldCameraToVertex = vVertexWorldPosition - cameraPosition;',
    '	vec3 viewCameraToVertex	= ( viewMatrix * vec4( worldCameraToVertex, 0.0 ) ).xyz;',
    '	viewCameraToVertex = normalize( viewCameraToVertex );',
    '	float intensity = pow( abs(coeficient + dot( vVertexNormal, viewCameraToVertex )), power );',
    '	gl_FragColor = vec4( glowColor, intensity );',
    '}'
].join('\n');

var coronaOutsideFragmentShader	= [
    
    'uniform vec3	glowColor;',
    'uniform float	coeficient;',
    'uniform float	power;',

    'varying vec3	vVertexNormal;',
    'varying vec3	vVertexWorldPosition;',

    'varying vec4	vFragColor;',

    'void main() {',
    '	vec3 worldVertexToCamera = cameraPosition - vVertexWorldPosition;',
    '	vec3 viewCameraToVertex	= ( viewMatrix * vec4( worldVertexToCamera, 0.0 ) ).xyz;',
    '	viewCameraToVertex = normalize( viewCameraToVertex );',
    '	float intensity = coeficient + dot( vVertexNormal, viewCameraToVertex );',
    '	if( intensity > 0.55 ) { intensity = 0.0; }',
    '	gl_FragColor = vec4( glowColor, intensity );',
    '}'
].join('\n');