/**
 * Evénements à créer lorsqu'un article est affiché
 */

function articleEvents(type, id) {
	
	// Ajout de tags

	$("#tags > form").submit(function() {
		
		setTags(this, type, id);

		return false;
	});

	// Ajouter ou supprimer des favoris

	$("#changeFavorite > a").click(function() {
		
		setFavorites(type, id);
		
		return false;
	});

	// Exportation PDF

	$("#pdfExport > a").click(function() {

		generatePDF();

		return false;
	});
}

