$(document).ready(function () {

	var scrollTop = $(".scrollTop");

	$(window).scroll(function () {

		var topPos = $(this).scrollTop();

		if (topPos > 100) {
			
			$(scrollTop).css("display", "flex");

		} else {
			
			$(scrollTop).css("display", "none");
		}

	});

	$(scrollTop).click(function () {
		
		$('html, body').animate({ scrollTop: 0 }, 800);
		
		return false;
	});

	$('.scrollTo').click(function () {
		
		var element = $($(this).attr("href")).position();
		
		$('html, body').animate({ scrollTop: element.top }, 500);
		
		return false;
	});
});