/**
 * Gestion des événements
 * 
 */
// Ajoute l'événement pour afficher un article

function showArticleEvent() {

	$(".showArticle").click(function() {

		if ($(this).attr("href").substring(1) == currentObject) {
			
			showArticle();
		}
	});
}

$(function() {
	
	// Fermeture du menu de paramètres
	
	closeMenu("#settings", "right");

	// Redimensionnement du canvas pour un affichage adaptatif

	window.addEventListener("resize", resize, false);

	// Surveille un changement d'URL

	window.onhashchange = locationHashChanged;

	// Changement de l'échelle de distance

	$('button[name="scaleDistance"]').click(function() {

		scaleDistanceInstruction = 1 / this.value;
		scaleDistanceChange = 1;
	});

	// Changement de l'échelle temporelle

	$('button[name="scaleTime"]').click(function() {

		scaleTimeInstruction = this.value;
		scaleTimeChange = 1;
	});

	// Evenement pour l'affichage d'un article
	
	showArticleEvent();
	
	// Fermeture de l'article

	$("#close-article").click(function() {

		if (articleOpen) {
			
			articleMustClose = 1;
			articleAnimation = 1;
		}
		
		return false;
	});

	// Fermeture des modales (popup)

	$(".close").click(function() {

		var idButton = $(this).attr("id");
		var popup = idButton.replace("close-", '');
		
		closePopup(popup);
		
		return false;
	});

	// Affichage des modales (popup)

	$(".action").click(function() {

		var idButton = $(this).attr("id");
		var popup = idButton.replace("button-", '');
		
		openPopup(popup);
		
		return false;
	});

	// Ouverture ou fermeture du menu de Navigation

	$("#navigationButton > a").click(function() {

		if (navigation) {
			
			closeMenu("nav", "left");
			
		} else {
			
			openMenu("nav", "left");
		}
		
		return false;
	});

	// Ouverture ou fermeture du menu de paramètres

	$("#settingsButton > a").click(function() {

		if (settings) {
			
			closeMenu("#settings", "right");
			
		} else {
			
			openMenu("#settings", "right");
		}
		
		return false;
	});

	// Choix de l'utilisation de la popup #profil (connexion ou mise à jour)

	$(".authenticate").click(function() {

		var idButton = $(this).attr("id");
		var popup = idButton.replace("button-",'');

		switch(popup) {

			case "connect":
	
				if (currentPopup == "profil") {
					
					currentPopup = "connect";
				}
				break;
	
			case "profil":
	
				if (currentPopup == "connect") {
					
					currentPopup = "profil";
				}
				break;
		}

		openPopup("profil");

		return false;
	});

	// Passage de la modale #profil à la modale #register

	$("#registerButton").click(function() {

		closePopup("profil");
		openPopup("register");
		
		return false;
	});

	// Connexion ou Mise à jour de l'utilisateur (POST modale #profil)

	$("#profil > .popup-body > form").submit(function() {

		postProfil(this);

		return false;
	});

	// Création d'un compte (POST modale #register)

	$("#register > .popup-body > form").submit(function() {

		updateUser(this);

		return false;
	});

	// Déconnexion

	$("#profil #disconnectButton a").click(function() {

		deconnect(this);

		return false;
	});

	// Lancer une recherche

	$('input[type="search"]').keyup(function() {

		startSearch = time;
		mustSearch = 1;
		search = this.value;
	});

	// Supprimer la recherche

	$("#removeSearch").click(function() {

		$("#search input").val('');
		$("#searchResults").css("display", "none");
		return false;
	});
	
	// Afficher ou cacher les constellations
	
	$("#constellations").click(function() {
		
		if (constellations) {
			
			removeConstellations();
			constellations = false;
			
		} else {
			
			initConstellations();
			constellations = true;
		}
	});

});