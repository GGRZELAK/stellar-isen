/**
 * Animation de l'univers 3D
 * Callback function()
 */

function animateUniverse()
{
	time = Date.now() * 0.001;
	chrono = time - startTime;

	if ( scaleTime != scaleTimeInstruction )
	{
		if ( scaleTimeChange )
		{
			scaleTimeChange = 0;
			scalTimeChangeStart = chrono;
			currentScaleTime = scaleTime;
		}

		scaleTime = scaleTimeInstruction;
	}

	// Rotation des astres sur eux-même
	
	for ( idPlanet in dataPlanets )
	{
		planet = dataPlanets[idPlanet];
		
		scene.getObjectByName( planet["name"] ).rotation.y += planet["rotationperiod"] * scaleTime / ( 24 * 3600 ) * ( chrono - prevChrono );
		
		if ( typeof dataSatellitesPlanets[planet["id"]] != "undefined" )
		{
			for ( idSatellite in dataSatellitesPlanets[planet["id"]] )
			{
				satellite = dataSatellitesPlanets[planet["id"]][idSatellite];
				
				scene.getObjectByName( satellite["name"] ).rotation.y += satellite["rotationperiod"] * scaleTime / ( 24 * 3600 ) * ( chrono - prevChrono );
			}
		}
	}

	// Effet sur Jupiter

	if ( scaleTime > 1 )
	{
		jupiterCloud.rotation.y += dataPlanets["Jupiter"]["rotationperiod"] * scaleTime / ( 24 * 3600 ) * 0.5 * ( chrono - prevChrono );
	}
	else
	{	
		jupiterCloud.rotation.y += 0.01 * ( chrono - prevChrono );
	}

	// Position de la caméra

	cameraDistance = Math.sqrt( Math.pow( camera.position.x, 2 ) + Math.pow( camera.position.y, 2 ) + Math.pow( camera.position.z, 2 ) );

	// Effet sur le soleil

	// On donne aux halos une taille proportionnelle à la distance de la caméra
	
	sunBigHaloSize = 2e-2 * cameraDistance + 8e6;
	sunBigHalo.scale.set( sunBigHaloSize, sunBigHaloSize, 1 );
	sunHaloSize = sunBigHaloSize + 5e6;
	sunHalo.scale.set( sunHaloSize, sunHaloSize, 1 );

	// On donne un effet de pulsation sur la couronne extérieure

	if ( scene.getObjectByName( "sunOutsideCorona" ) )
	{
		sunOutsideCoronaSize = 1e3 * Math.sin( chrono / 3 ) + 1e5 * Math.sin( chrono / 2 ) + 1e4 * Math.sin( chrono ) + sunOutsideCoronaReference;
		sunOutsideCorona.scale.set( sunOutsideCoronaSize, sunOutsideCoronaSize, 1 );
	}

	// Calcul de l'opacity des effets du soleil en fonction de la distance la caméra

	a = ( 0 - 1 ) / ( 50e6 - 10e6 );
	b = 1 + Math.abs( a ) * 10e6;
	sunOpacity = a * cameraDistance + b;
	if ( sunOpacity > 1 ) sunOpacity = 1;
	else if ( sunOpacity < 0) sunOpacity = 0;

	aScale = ( 0 - 1 ) / ( 50e6 * scaleDistance - 10e6 * scaleDistance );
	bScale = 1 + Math.abs( aScale ) * 10e6 * scaleDistance;
	sunScaleOpacity = aScale * cameraDistance + bScale;
	if ( sunScaleOpacity > 1 ) sunScaleOpacity = 1;
	else if ( sunScaleOpacity < 0 ) sunScaleOpacity = 0;

	// On donne un effet de pulsation sur la couleur quand on est proche du soleil

	sunRedFusion = 1;
	sunGreenFusion = 0.5 * Math.sin( chrono ) * sunOpacity + ( -sunOpacity + 1 );
	sunBlueFusion = 0.5 * Math.sin( chrono ) * sunOpacity + ( -sunOpacity + 1 );
	sunFusion.material.color.setRGB( sunRedFusion, sunGreenFusion, sunBlueFusion );

	// On modifie la couleur du grand halo selon la distance de la caméra

	sunRedBigHalo = -0.16 * sunOpacity + 1;
	sunGreenBigHalo = -0.26 * sunOpacity + 1;
	sunBlueBigHalo = -0.38 * sunOpacity + 1;
	sunBigHalo.material.color.setRGB( sunRedBigHalo, sunGreenBigHalo, sunBlueBigHalo );

	// Si la caméra est à moins de 50e6 Km du soleil, on ajoute les effets au soleil

	if ( sunOpacity > 0 )
	{
		if ( !scene.getObjectByName( "sunOutsideCorona" ) )
		{
			scene.add( sunOutsideCorona );
			sunOutsideCorona.name = "sunOutsideCorona";
		}
		if ( !scene.getObjectByName( "sun" ) )
		{
			scene.add( sun );
			sun.name = "sun";
		}

		sunOutsideCorona.material.opacity = sunOpacity;
		sun.material.opacity = sunOpacity;

		if ( sunScaleOpacity > 0 )
		{
			if ( !scene.getObjectByName( "sunInsideCorona" ) )
			{
				scene.add( sunInsideCorona );
				sunInsideCorona.name = "sunInsideCorona";
			}

			sunInsideCorona.material.opacity = sunOpacity;
		}
	}

	// Si la caméra est à plus de 50e6 Km du soleil, on supprime les effets sur le soleil

	if ( sunOpacity <= 0 )
	{
		if ( scene.getObjectByName( "sunOutsideCorona" ) )
		{
			sunOutsideCorona.name = null;
			scene.remove( sunOutsideCorona );
		}
		if ( scene.getObjectByName( "sun" ) )
		{
			sun.name = null;
			scene.remove( sun );
		}
		if ( scene.getObjectByName( "sunInsideCorona" ) )
		{
			sunInsideCorona.name = null;
			scene.remove( sunInsideCorona );
		}
	}

	// Mise à l'échelle

	if ( scaleDistance != scaleDistanceInstruction )
	{
		if ( scaleDistanceChange )
		{
			scaleDistanceChange = 0;
			scaleDistanceChangeStart = chrono;
			currentScaleDistance = scaleDistance;
		}

		// TODO Faire varier progressivement le changement d'échelle

		scaleDistance = scaleDistanceInstruction;

		// Mise à jour de la position des astres
		
		for ( idPlanet in dataPlanets )
		{
			planet = dataPlanets[idPlanet];
			
			scene.getObjectByName( planet["name"] + "System" ).position.z = dataSun["Sun"]["radius"] + planet["stardistance"] * scaleDistance + planet["radius"];
			
			if ( typeof dataSatellitesPlanets[planet["id"]] != "undefined" )
			{	
				for ( idSatellite in dataSatellitesPlanets[planet["id"]] )
				{
					satellite = dataSatellitesPlanets[planet["id"]][idSatellite];
					scene.getObjectByName( satellite["name"] ).position.z = planet["radius"] + satellite["planetdistance"] * scaleDistance + satellite["radius"];
				}
			}
		}


		// Mise à jour de la position de la caméra
		// TODO Replacer la caméra lorsqu'on est ailleurs que sur une planète
		if ( currentType == "planets" )
		{
			camera.position.x = dataPlanets[currentObject]["radius"] * 3;
			camera.position.z = dataSun["Sun"]["radius"] + dataPlanets[currentObject]["stardistance"] * scaleDistance + dataPlanets[currentObject]["radius"];
			controls.target.z = dataSun["Sun"]["radius"] + dataPlanets[currentObject]["stardistance"] * scaleDistance + dataPlanets[currentObject]["radius"];
		}

	}

	// Déplacement vers le nouvel astre

	if ( currentObject != instructionObject )
	{
		// TODO Ne pas partir d'un astre courant mais de la position courrante de la caméra
		// Si un article est ouvert, on le ferme avant de se déplacer vers le nouvel astre

		if ( articleOpen && !articleMustClose )
		{
			articleMustClose = 1;
			articleAnimation = 1;
		}
		else if ( articleOpen || !articleMustClose )
		{
			if ( move )
			{
				if ( instructionType == "satellites" ) alpha = degreeToRadian(dataSatellites[instructionObject]["ecliptictilt"]);
				else alpha = 0;
				
				if ( instructionType == "planets" ) object = scene.getObjectByName( instructionObject + "System" );
				else object = scene.getObjectByName( instructionObject );
				
				objectInstructionX = object.position.x + instructionData[instructionObject]["radius"] * 3;
				objectInstructionY = -object.position.z * Math.sin( alpha ) + object.position.y * Math.cos( alpha );
				objectInstructionZ = object.position.y * Math.sin( alpha ) + object.position.z * Math.cos( alpha );

				if ( instructionType == "satellites" )
				{
					objectInstructionX += object.parent.parent.position.x;
					objectInstructionY += object.parent.parent.position.y;
					objectInstructionZ += object.parent.parent.position.z;
				}

				deltaX = deltaXs = objectInstructionX - camera.position.x;
				deltaY = deltaYs = objectInstructionY - camera.position.y;
				deltaZ = deltaZs = objectInstructionZ - camera.position.z;

				move = 0;
			}

			if ( Math.abs( deltaX ) >= Math.abs( deltaXs ) / 2 ) vitesseCameraX += deltaX / 200;
			else
			{
				vitesseCameraX -= deltaX / 200;
				controls.target.x = 0;
			}

			if ( Math.abs( deltaY ) >= Math.abs( deltaYs ) / 2 ) vitesseCameraY += deltaY / 200;
			else
			{
				vitesseCameraY -= deltaY / 200;
				controls.target.y = objectInstructionY;
			}

			if ( Math.abs( deltaZ ) >= Math.abs( deltaZs ) / 2 ) vitesseCameraZ += deltaZ / 200;
			else
			{
				vitesseCameraZ -= deltaZ / 200;
				controls.target.z = objectInstructionZ;
			}

			deltaX -= vitesseCameraX * ( chrono - prevChrono );
			deltaY -= vitesseCameraY * ( chrono - prevChrono );
			deltaZ -= vitesseCameraZ * ( chrono - prevChrono );

			if ( Math.sign( deltaX ) != Math.sign( deltaXs ) )
			{
				deltaX = 0;
				vitesseCameraX = 0;
			}
			if ( Math.sign( deltaY ) != Math.sign( deltaYs ) )
			{
				deltaY = 0;
				vitesseCameraY = 0;
			}
			if ( Math.sign( deltaZ ) != Math.sign( deltaZs ) )
			{
				deltaZ = 0;
				vitesseCameraZ = 0;
			}

			// Le déplacement est terminé

			if ( deltaX == 0 && deltaY == 0 && deltaZ == 0 )
			{
				currentObject = instructionObject;
				currentData = instructionData;
				currentType = instructionType;
				
				reloadNavigation();

				// Ouverture automatique de l'article

				if ( !articleMustClose ) showArticle();
			}

			camera.position.x = objectInstructionX - deltaX;
			camera.position.y = objectInstructionY - deltaY;
			camera.position.z = objectInstructionZ - deltaZ;
		}
	}

	// L'article doit s'ouvrir

	if ( articleMustOpen )
	{
		// TODO Permettre l'ouverture d'un article sans que l'objet 3D existe. Le volet doit donc s'ouvrir sans que la caméra ne se se déplace.
		// TODO Ne pas déplacer le controle target mais déplacer le caméra lookAt
		// Animation en cours

		if ( articleAnimation )
		{
			vitesseCamera = currentData[currentObject]["radius"] * 0.1;

			camera.position.z -= vitesseCamera;
			controls.target.z -= vitesseCamera;

			if ( Math.abs( objectInstructionZ - camera.position.z ) >= currentData[currentObject]["radius"] )
			{
				// L'animation est terminée

				articleAnimation = 0;
			}
		}
		else
		{
			// L'article est ouvert

			articleMustOpen = 0;
			articleOpen = 1;

			// Affichage de l'article

			displayArticle( currentType, currentData[currentObject]["id"] );
		}
	}

	// L'article doit se fermer

	if ( articleMustClose )
	{
		// Le volet doit se fermer avant l'animation

		if ( articleOpen )
		{
			// On cache le volet article

			$( "#article" ).css( "display", "none" );
			articleOpen = 0;
		}

		// Animation en cours

		if ( articleAnimation )
		{
			vitesseCamera = currentData[currentObject]["radius"] * 0.1;

			camera.position.z += vitesseCamera;
			controls.target.z += vitesseCamera;

			if ( objectInstructionZ - camera.position.z <= 0 )
			{
				camera.position.z = controls.target.z = objectInstructionZ;

				// L'animation est terminée

				articleAnimation = 0;
			}
		}
		else
		{
			// L'article est fermé

			$( "#seeArticle .scrollTo" ).css( "display", "none" );
			articleMustClose = 0;
		}
	}

	// Recherche
	// On attend une demi seconde après chaque appui avant de lancer la recherche
	// On ne recheche qu'un mot >= 3 caractères

	if ( search )
	{
		if ( search.length >= 3 )
		{
			if ( mustSearch && mustCloseSearch != 1 ) $( "#searchLoader" ).css( "display", "flex" );

			if ( time - startSearch >= 1/2 )
			{
				if ( mustSearch ) doSomeSearch();
			}
		}
		else if ( mustCloseSearch )
		{
			$( "#searchLoader" ).css( "display", "none" );
			$( "#searchResults" ).css( "display", "none" );
			mustCloseSearch = 0;
		}
	}

	prevChrono = chrono;

	controls.update();

	requestAnimationFrame( animateUniverse );
	renderer.render( scene, camera );
}