-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Mar 18 Décembre 2018 à 14:14
-- Version du serveur :  5.7.24-0ubuntu0.18.04.1
-- Version de PHP :  7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `stellar-isen`
--
CREATE DATABASE IF NOT EXISTS `stellar-isen` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `stellar-isen`;

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `idip` int(11) NOT NULL,
  `author` varchar(255) NOT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `comment` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `concepts`
--

CREATE TABLE `concepts` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `article` longtext NOT NULL,
  `tags` varchar(255) NOT NULL,
  `referencetable` varchar(255) DEFAULT NULL,
  `idelement` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `constellations`
--

CREATE TABLE `constellations` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `con` varchar(10) NOT NULL,
  `article` longtext NOT NULL,
  `tags` varchar(255) NOT NULL,
  `numberlinks` int(11) NOT NULL,
  `links` text NOT NULL COMMENT 'Identifiant Hipparcos'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `dictionary`
--

CREATE TABLE `dictionary` (
  `id` int(11) NOT NULL,
  `fr` varchar(255) NOT NULL,
  `en` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `favorites`
--

CREATE TABLE `favorites` (
  `id` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `referencetable` varchar(255) NOT NULL,
  `idelement` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `hyg`
--

CREATE TABLE `hyg` (
  `id` int(11) NOT NULL,
  `hip` int(11) DEFAULT NULL,
  `hd` int(11) DEFAULT NULL,
  `hr` int(11) DEFAULT NULL,
  `gl` varchar(10) DEFAULT NULL,
  `bf` varchar(10) DEFAULT NULL,
  `proper` varchar(30) DEFAULT NULL,
  `ra` double DEFAULT NULL,
  `declination` double DEFAULT NULL,
  `dist` double DEFAULT NULL,
  `pmra` double DEFAULT NULL,
  `pmdec` double DEFAULT NULL,
  `rv` double DEFAULT NULL,
  `mag` double DEFAULT NULL,
  `absmag` double DEFAULT NULL,
  `spect` varchar(20) DEFAULT NULL,
  `ci` double DEFAULT NULL,
  `x` double DEFAULT NULL,
  `y` double DEFAULT NULL,
  `z` double DEFAULT NULL,
  `vx` double DEFAULT NULL,
  `vy` double DEFAULT NULL,
  `vz` double DEFAULT NULL,
  `rarad` double DEFAULT NULL,
  `decrad` double DEFAULT NULL,
  `pmrarad` double DEFAULT NULL,
  `pmdecrad` double DEFAULT NULL,
  `bayer` varchar(10) DEFAULT NULL,
  `flam` double DEFAULT NULL,
  `con` varchar(10) DEFAULT NULL,
  `comp` double DEFAULT NULL,
  `comp_primary` double DEFAULT NULL,
  `base` varchar(10) DEFAULT NULL,
  `lum` double DEFAULT NULL,
  `var` varchar(10) DEFAULT NULL,
  `var_min` double DEFAULT NULL,
  `var_max` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ips`
--

CREATE TABLE `ips` (
  `id` int(11) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `linksuserip`
--

CREATE TABLE `linksuserip` (
  `iduser` int(11) NOT NULL,
  `idip` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `objects`
--

CREATE TABLE `objects` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `axialtilt` int(11) NOT NULL,
  `ecliptictilt` int(11) NOT NULL,
  `rotationperiod` float NOT NULL,
  `orbitalperiod` float NOT NULL,
  `article` longtext NOT NULL,
  `tags` varchar(255) NOT NULL,
  `referencetable` varchar(255) NOT NULL,
  `idelement` int(11) NOT NULL,
  `elementdistance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `planets`
--

CREATE TABLE `planets` (
  `id` int(11) NOT NULL,
  `idstar` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) NOT NULL COMMENT 'Anglais',
  `stardistance` double NOT NULL COMMENT 'Kilomètres',
  `orbitalperiod` double NOT NULL COMMENT 'Jours',
  `ecliptictilt` double NOT NULL COMMENT 'Degrés',
  `radius` double NOT NULL COMMENT 'Kilomètres',
  `rotationperiod` double NOT NULL COMMENT 'Jours',
  `axialtilt` double NOT NULL COMMENT 'Degrés',
  `article` longtext NOT NULL COMMENT 'HTML',
  `tags` varchar(255) DEFAULT NULL COMMENT 'un,deux'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `satellites`
--

CREATE TABLE `satellites` (
  `id` int(11) NOT NULL,
  `idplanet` int(11) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'Anglais',
  `planetdistance` double NOT NULL COMMENT 'Kilomètres',
  `orbitalperiod` double NOT NULL COMMENT 'Jours',
  `ecliptictilt` double NOT NULL COMMENT 'Degrés',
  `radius` double NOT NULL COMMENT 'Kilomètres',
  `rotationperiod` double NOT NULL COMMENT 'Jours',
  `axialtilt` double NOT NULL COMMENT 'Degrés',
  `article` longtext NOT NULL COMMENT 'HTML',
  `tags` varchar(255) NOT NULL COMMENT 'un,deux'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `stars`
--

CREATE TABLE `stars` (
  `id` int(11) NOT NULL,
  `radius` double DEFAULT NULL,
  `article` longtext,
  `tags` varchar(255) DEFAULT NULL,
  `hip` int(11) DEFAULT NULL,
  `hd` int(11) DEFAULT NULL,
  `hr` int(11) DEFAULT NULL,
  `gl` varchar(10) DEFAULT NULL,
  `bf` varchar(10) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `ra` double DEFAULT NULL,
  `declination` double DEFAULT NULL,
  `dist` double DEFAULT NULL,
  `pmra` double DEFAULT NULL,
  `pmdec` double DEFAULT NULL,
  `rv` double DEFAULT NULL,
  `mag` double DEFAULT NULL,
  `absmag` double DEFAULT NULL,
  `spect` varchar(20) DEFAULT NULL,
  `idstarcolor` int(11) NOT NULL,
  `ci` double DEFAULT NULL,
  `x` double DEFAULT NULL,
  `y` double DEFAULT NULL,
  `z` double DEFAULT NULL,
  `vx` double DEFAULT NULL,
  `vy` double DEFAULT NULL,
  `vz` double DEFAULT NULL,
  `rarad` double DEFAULT NULL,
  `decrad` double DEFAULT NULL,
  `pmrarad` double DEFAULT NULL,
  `pmdecrad` double DEFAULT NULL,
  `bayer` varchar(10) DEFAULT NULL,
  `flam` double DEFAULT NULL,
  `con` varchar(10) DEFAULT NULL,
  `comp` double DEFAULT NULL,
  `comp_primary` double DEFAULT NULL,
  `base` varchar(10) DEFAULT NULL,
  `lum` double DEFAULT NULL,
  `var` varchar(10) DEFAULT NULL,
  `var_min` double DEFAULT NULL,
  `var_max` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `starscolor`
--

CREATE TABLE `starscolor` (
  `id` int(11) NOT NULL,
  `spect` varchar(20) NOT NULL,
  `r` int(3) NOT NULL,
  `g` int(3) NOT NULL,
  `b` int(3) NOT NULL,
  `hexa` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `password` varchar(20) NOT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `registration` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `administrator` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idIP` (`idip`);

--
-- Index pour la table `concepts`
--
ALTER TABLE `concepts`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `constellations`
--
ALTER TABLE `constellations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `dictionary`
--
ALTER TABLE `dictionary`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idUser` (`iduser`);

--
-- Index pour la table `hyg`
--
ALTER TABLE `hyg`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ips`
--
ALTER TABLE `ips`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `linksuserip`
--
ALTER TABLE `linksuserip`
  ADD PRIMARY KEY (`iduser`,`idip`),
  ADD KEY `idIP` (`idip`);

--
-- Index pour la table `objects`
--
ALTER TABLE `objects`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `planets`
--
ALTER TABLE `planets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `planets_ibfk_1` (`idstar`);

--
-- Index pour la table `satellites`
--
ALTER TABLE `satellites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idPlanet` (`idplanet`);

--
-- Index pour la table `stars`
--
ALTER TABLE `stars`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `starscolor`
--
ALTER TABLE `starscolor`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `concepts`
--
ALTER TABLE `concepts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `constellations`
--
ALTER TABLE `constellations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT pour la table `dictionary`
--
ALTER TABLE `dictionary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT pour la table `ips`
--
ALTER TABLE `ips`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `objects`
--
ALTER TABLE `objects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `planets`
--
ALTER TABLE `planets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `satellites`
--
ALTER TABLE `satellites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `starscolor`
--
ALTER TABLE `starscolor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`idip`) REFERENCES `ips` (`id`);

--
-- Contraintes pour la table `favorites`
--
ALTER TABLE `favorites`
  ADD CONSTRAINT `favorites_ibfk_1` FOREIGN KEY (`iduser`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `linksuserip`
--
ALTER TABLE `linksuserip`
  ADD CONSTRAINT `linksuserip_ibfk_1` FOREIGN KEY (`idip`) REFERENCES `ips` (`id`),
  ADD CONSTRAINT `linksuserip_ibfk_2` FOREIGN KEY (`iduser`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `planets`
--
ALTER TABLE `planets`
  ADD CONSTRAINT `planets_ibfk_1` FOREIGN KEY (`idstar`) REFERENCES `stars` (`id`);

--
-- Contraintes pour la table `satellites`
--
ALTER TABLE `satellites`
  ADD CONSTRAINT `satellites_ibfk_1` FOREIGN KEY (`idplanet`) REFERENCES `planets` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
